# Copyright (c) 2018, Lachlan Nicholson, ARC Centre of Excellence for Robotic Vision, Queensland University of Technology (QUT)
# All rights reserved.
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Created by Lachlan on 12/12/18.
#

# import standard libraries
from __future__ import print_function
import numpy as np
import json, sys, os, argparse, cv2
from scipy import misc
from copy import deepcopy

# import shared libraries
sys.dont_write_bytecode = True
import scenenet_pb2 as sn

def _iou(boxA, boxB):
	# determine the (x, y)-coordinates of the intersection rectangle
	xA = max(boxA[0], boxB[0])
	yA = max(boxA[1], boxB[1])
	xB = min(boxA[2], boxB[2])
	yB = min(boxA[3], boxB[3])

	# compute the area of intersection rectangle
	interArea = max(0, max(0,(xB - xA + 1)) * max(0,(yB - yA + 1)))

	# compute the area of both the prediction and ground-truth boxes
	boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
	boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

	# compute the intersection over union by taking the intersection
	# area and dividing it by the sum of prediction + ground-truth
	# areas - the interesection area
	iou = interArea / float(boxAArea + boxBArea - interArea)

	if iou < 0.0:
		print('NEGATIVE IOU')
		exit()
	return iou

def cv2_draw_box(image, box, color=(0,0,255), thickness=2):
	cv2.rectangle(image, (int(box[0]),int(box[1])), (int(box[2]),int(box[3])), color, thickness)

def load_detections(detections_path):
	data = json.load(open(detections_path))
	return data

def draw_detections(detections_path, images_folder):
	data = load_detections(detections_path)
	for image_detections in data['images']:
		print(image_detections['image_fn'])
		image = cv2.imread(os.path.join(images_folder, image_detections['image_fn']))

		for detection in image_detections['detections']:
			cv2_draw_box(image, detection['box'], (0,0,255), 2)
		cv2.imshow('test', image)
		cv2.waitKey(0)

def main(detections_path, images_folder):
	data = load_detections(detections_path)
	instances_folder = images_folder.replace('/photo','/instance')
	protobuf_path = '/media/feyre/DATA1/Datasets/SceneNetRGBD/pySceneNetRGBD/data/scenenet_rgbd_val.pb'
	render_path = '0/1'

	wnid_to_coco = json.load(open('./wnid_to_COCO.json'))
	# with open('detections.json', 'w') as f:
	# 	json.dump(data, f)

	trajectories = sn.Trajectories()
	with open(protobuf_path,'rb') as f:
		trajectories.ParseFromString(f.read())

	instance_to_wnid = {}
	for trajectory in trajectories.trajectories:
		if (str(trajectory.render_path) == render_path):
			for instance in trajectory.instances:
				if (str(instance.semantic_wordnet_id) != ''):
					instance_to_wnid[instance.instance_id] = str(instance.semantic_wordnet_id)
					# instance_to_wnid[int(instance.instance_id)] = str(instance.semantic_english)

	# print(instance_to_wnid)
	# exit()


	instances = []

	# sort detections by most overlapping instance
	for image_detections in data['images']:
		instance_path = os.path.join(instances_folder, image_detections['image_fn'].replace('.jpg','.png'))
		instance_image = misc.imread(instance_path)
		fim = instance_image.astype('float')
		sim = ((fim - fim.min()) * (1/(fim.max() - fim.min()) * 255)).astype('uint8')
		rim = np.stack((sim,)*3, axis=-1)
		rgb_image = cv2.imread(os.path.join(images_folder, image_detections['image_fn']))
		rim = rgb_image

		for detection in image_detections['detections']:
			img = deepcopy(rim)
			cv2_draw_box(img, detection['box'], (0,0,255), 2)

			instance_ids = np.unique(instance_image)
			ious = np.zeros(len(instance_ids))
			boxes = []
			for index, instance_id in enumerate(instance_ids):
				instance_mask = (instance_image == instance_id)
				horizontal_mask = np.any(instance_mask, axis=0)
				vertical_mask = np.any(instance_mask, axis=1)
				xmin, xmax = np.where(horizontal_mask)[0][[0,-1]]
				ymin, ymax = np.where(vertical_mask)[0][[0,-1]]
				if (xmin == xmax or ymin == ymax):
					# print('this wasnt supposed to happen')
					pass
				box = [xmin, ymin, xmax, ymax]
				iou = _iou(detection['box'], box)
				ious[index] = iou
				boxes.append(box)


			# threshold associations
			ious[ious<0.5] = 0
			if (not np.any(ious)):
				# print('no gt left')
				continue
			max_index = ious.argmax()
			max_iou = ious.max()
			max_instance_id = instance_ids[max_index]
			cv2_draw_box(img, boxes[max_index], (255,0,0), 2)

			print('associated instance {} with iou {:.3f} as {}'.format(max_instance_id, max_iou, instance_to_wnid[max_instance_id]))
			object_info = wnid_to_coco[instance_to_wnid[max_instance_id].lstrip('0')]
			if (object_info['coco'] == 'empty'):
				detection_class = data['classes'][np.argmax(detection['scores'])]
				print('[{}] no translation. Suggested ({})'.format(object_info['name'], detection_class))

			# detection_index = np.argmax(detection['scores'])
			# detection_class = data['classes'][detection_index]
			# print('detection: {}:{}'.format(detection_index,detection_class))


			cv2.imshow('test', img)
			cv2.waitKey(0)



if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	# parser.add_argument('--run', help='cli flag to run new experiment', action='store_true')
	parser.add_argument('--detections_path', help='relative path to detections file',
											default='/home/feyre/git_ws/object_detectors/pytorch-yolo-v3-ayooshka-modified/detections/val_0_1.json')
	parser.add_argument('--images_folder', help='relative path to images folder',
											default='/media/feyre/DATA1/Datasets/SceneNetRGBD/pySceneNetRGBD/data/val/0/1/photo')
	args = parser.parse_args()

	main(args.detections_path, args.images_folder)
