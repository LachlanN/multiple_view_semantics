# Copyright (c) 2018, Lachlan Nicholson, ARC Centre of Excellence for Robotic Vision, Queensland University of Technology (QUT)
# All rights reserved.
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Created by Lachlan on 12/12/18.
#

# import standard libraries
from __future__ import print_function
import time as timelib
import numpy as np
import sys, os
import importlib

import torch
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms as transforms
import torch.nn.functional as F
from PIL import Image, ImageDraw

# import shared libraries
sys.dont_write_bytecode = True
from conversions import NYU_13_CLASSES, WNID_TO_NYU_13

# debugging
import matplotlib.pyplot as plt


def natural_sort(l):
	convert = lambda text: int(text) if text.isdigit() else text.lower()
	alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
	return sorted(l, key = alphanum_key)




class SceneNetDataset(torch.utils.data.Dataset):
	def __init__(self, root_dir, megabatch_size, major_seed, minor_seed, image_size=416, augment=True, multiscale=True):
		self.image_size = image_size
		self.augment = augment
		self.multiscale = multiscale
		self.root_dir = root_dir

		# load protobuf definition
		self.sn = self.load_protobuf_reader('/media/feyre/DATA1/Datasets/SceneNetRGBD/pySceneNetRGBD/scenenet_pb2.py')

		# get protobuf folder from dataset name
		self.dataset_name = root_dir.split('/')[-1] if (not root_dir.endswith('/')) else root_dir.split('/')[-2]
		self.protobuf_folder = os.path.join(os.path.realpath(os.path.join(root_dir, '../')), '{}_protobufs'.format(self.dataset_name))

		# collect image paths
		render_folders = [fn for fn in os.listdir(root_dir)
								if os.path.isdir(os.path.join(root_dir, fn))]

		video_paths = [os.path.join(root_dir, pf, fn) for pf in render_folders
									for fn in os.listdir(os.path.join(root_dir, pf))
										if (not '.' in fn)]

		image_paths = [os.path.join(vp, 'photo', '{}.jpg'.format(n))
								for vp in video_paths
									for n in range(0,7476,25)]

		# randomly split image paths into mega batches
		np.random.seed(major_seed)
		np.random.shuffle(image_paths)
		self.batch_paths = [image_paths[i:i+megabatch_size] for i in range(0,len(image_paths),megabatch_size)] 		# split into n groups d = [images[i::n] for i in range(n)]
		for batch in self.batch_paths:
			np.random.seed(minor_seed)
			np.random.shuffle(batch)

		self.video_to_info = {}

		print('Loaded ({} videos) and ({} images) into ({} batches)'.format(len(video_paths), len(image_paths), len(self.batch_paths)))


	def get_video_info(self, video_path):
		if (not video_path in self.video_to_info):
			render_folder = video_path.split('/')[-2]
			protobuf_name = 'scenenet_rgbd_{}_{}.pb'.format(self.dataset_name, render_folder)
			self.load_protobuf(protobuf_name)
		instance_to_nyu = self.video_to_info[video_path]
		return instance_to_nyu


	def load_protobuf(self, protobuf_name):
		trajectories = self.sn.Trajectories()
		with open(os.path.join(self.protobuf_folder, protobuf_name),'rb') as f:
			trajectories.ParseFromString(f.read())

		for trajectory in trajectories.trajectories:
			render_path = str(trajectory.render_path)
			video_path = os.path.join(self.root_dir, render_path)

			instance_info = {}
			for instance in trajectory.instances:
				if (instance.semantic_wordnet_id != ''):
					instance_info[int(instance.instance_id)] = {'wnid': str(instance.semantic_wordnet_id),
																'name': str(instance.semantic_english),
																'nyu': (WNID_TO_NYU_13[str(instance.semantic_wordnet_id)] - 1)}
			self.video_to_info[video_path] = instance_info


	def load_protobuf_reader(self, protobuf_def_path='/media/feyre/DATA1/Datasets/SceneNetRGBD/pySceneNetRGBD/scenenet_pb2.py'):
		# extract folder / file information
		protobuf_def_folder = protobuf_def_path.replace(protobuf_def_path.split('/')[-1], '')
		protobuf_def_file = protobuf_def_path.replace(protobuf_def_folder, '')

		# load protobuf definition
		sys.path.append(os.path.realpath(protobuf_def_folder))
		sn = importlib.import_module(protobuf_def_file.replace('.py',''))
		return sn


	def __len__(self):
		return len(self.batch_paths)

	def __getitem__(self, index):
		return SceneNetImages(self.batch_paths[index % len(self)], self.get_video_info, self.image_size, self.augment, self.multiscale)

	def get_item(self, index):
		return self.batch_paths[index % len(self)]




class SceneNetImages(torch.utils.data.Dataset):
	def __init__(self, image_paths, get_video_info, image_size=416, augment=True, multiscale=True):
		self.image_paths = image_paths
		self.get_video_info = get_video_info

		self.image_size = image_size
		self.augment = augment
		self.multiscale = multiscale
		self.min_size = self.image_size - 3 * 32
		self.max_size = self.image_size + 3 * 32
		self.batch_count = 0

	def __len__(self):
		return len(self.image_paths)

	def __getitem__(self, index):
		'''
			Returns [0, label, %cx, %cy, %w, %h]
		'''

		# load rgb image
		rgb_path = self.image_paths[index]
		image = transforms.ToTensor()(Image.open(rgb_path).convert('RGB'))

		# load instance image
		instance_path = rgb_path.replace('photo','instance').replace('.jpg','.png')
		instance_image = np.array(Image.open(instance_path))

		# calculate instance boxes
		instance_boxes = self.get_boxes(instance_image, min_size=5)

		# convert to labeled boxes
		video_path = rgb_path.split('photo')[0][:-1]
		instance_to_nyu = self.get_video_info(video_path)
		label_boxes = self.add_labels(instance_boxes, instance_to_nyu)

		# pad image to square
		image, pad = self.pad_to_square(image, 0)
		_, padded_h, padded_w = image.shape

		# adjust boxes for padding
		targets = None
		if (len(label_boxes) > 0):
			targets = self.condition_boxes(label_boxes, pad, padded_w, padded_h)

		# resize image
		# image = self.resize(image, self.image_size)
		# padded_boxes = self.resize_boxes(padded_boxes, padded_w, self.image_size)

		# apply augmentations
		if self.augment:
			if np.random.random() < 0.5:
				image, targets = self.horisontal_flip(image, targets)

		return rgb_path, image, targets


	def horisontal_flip(self, images, targets):
		images = torch.flip(images, [-1])
		if (not targets is None):
			targets[:, 2] = 1 - targets[:, 2]
		return images, targets

	def resize_boxes(self, boxes, old_size, new_size):
		_boxes = boxes.astype('float')
		_boxes[:, 2:] *= float(new_size)/old_size
		return _boxes

	def resize(self, image, size):
		image = F.interpolate(image.unsqueeze(0), size=size, mode="nearest").squeeze(0)
		return image

	def condition_boxes(self, boxes, pad, padded_w, padded_h):
		'''
			converts 	[label, xmin, ymin, xmax, ymax]
			to 			[0, label, %cx, %cy, %w, %h]
		'''
		# add column
		targets = torch.zeros((len(boxes), 6))

		# pad boxes
		boxes[:, 1] += pad[0]
		boxes[:, 2] += pad[2]
		boxes[:, 3] += pad[1]
		boxes[:, 4] += pad[3]

		# normalize boxes
		targets[:, 2] = ((boxes[:, 1] + boxes[:, 3]) / 2) / padded_w
		targets[:, 3] = ((boxes[:, 2] + boxes[:, 4]) / 2) / padded_h
		targets[:, 4] = (boxes[:, 3] - boxes[:, 1]) / padded_w
		targets[:, 5] = (boxes[:, 4] - boxes[:, 2]) / padded_h
		targets[:, 1] = boxes[:,0]
		return targets

	def pad_to_square(self, image, pad_value):
		c, h, w = image.shape
		dim_diff = np.abs(h - w)
		pad1, pad2 = dim_diff // 2, dim_diff - dim_diff // 2
		pad = (0, 0, pad1, pad2) if h <= w else (pad1, pad2, 0, 0)
		image = F.pad(image, pad, "constant", value=pad_value)
		return image, pad

	def add_labels(self, instance_boxes, instance_to_nyu):
		'''
			Converts an instance image into suitable class boxes
			Returns linear structure [label, x1, y1, x2, y2] (n_instances, 5):
			Requires: self.instance_to_nyu
		'''
		label_boxes = []
		for instance_box in instance_boxes:
			if (int(instance_box[0]) in instance_to_nyu):
				label = instance_to_nyu[int(instance_box[0])]['nyu']
				label_box = [label] + instance_box[1:].tolist()
				label_boxes.append(label_box)

		return torch.FloatTensor(label_boxes)

	def get_boxes(self, instance_image, min_size=5):
		'''
			Converts an instance image into suitable class boxes
			Returns linear structure [instance_id, x1, y1, x2, y2] (n_instances, 5):
		'''
		boxes = []
		for instance_id in np.unique(instance_image):

			# get masks
			instance_mask = (instance_image == instance_id)
			horizontal_mask = np.any(instance_mask, axis=0)
			vertical_mask = np.any(instance_mask, axis=1)

			# calculate bounds
			xmin, xmax = np.where(horizontal_mask)[0][[0,-1]]
			ymin, ymax = np.where(vertical_mask)[0][[0,-1]]

			# filter small boxes
			if ((xmax-xmin) > min_size) and ((ymax-ymin) > min_size):
				box = [instance_id, xmin, ymin, xmax, ymax]
				boxes.append(box)

		return torch.FloatTensor(boxes)


	def normalize_box(self, box, w, h):
		'''
		Converts [xmin,ymin,xmax,ymax] -> [%cx,%cy,%w,%h]
		'''
		nbox = [float(box[0]+(box[2]-box[0])/2.0) / w,
				float(box[1]+(box[3]-box[1])/2.0) / h,
				float(box[2]-box[0]) / w,
				float(box[3]-box[1]) / h]
		return nbox
	def denormalize_box(self, nbox, w, h):
		'''
		Converts [%cx,%cy,%w,%h] -> [xmin, ymin, xmax, ymax]
		'''
		box = [int(round((nbox[0]-(nbox[2])/2.0) * w)),
			   int(round((nbox[1]-(nbox[3])/2.0) * h)),
			   int(round((nbox[0]+(nbox[2])/2.0) * w)),
			   int(round((nbox[1]+(nbox[3])/2.0) * h))]
		return box


	def collate_fn(self, batch):
		paths, imgs, targets = list(zip(*batch))
		# Remove empty placeholder targets
		targets = [boxes for boxes in targets if boxes is not None]
		# Add sample index to targets
		for i, boxes in enumerate(targets):
			boxes[:, 0] = i
		targets = torch.cat(targets, 0)
		# Selects new image size every tenth batch
		# print('ms', self.multiscale)
		# print('f1', (self.batch_count % 10 == 0))
		# print('flag', ((self.multiscale) and (self.batch_count % 10 == 0)))
		if ((self.multiscale) and (self.batch_count % 10 == 0)):
			self.image_size = random.choice(range(self.min_size, self.max_size + 1, 32))
		# Resize images to input shape
		imgs = torch.stack([self.resize(img, self.image_size) for img in imgs])
		self.batch_count += 1
		return paths, imgs, targets










	# '''
	# args:
	# 	- megabatch_size
	# 	- batch_size
	# 	- image_size
	# 	- augment
	# 	- multiscale
	# '''




if __name__ == '__main__':

	dataset = SceneNetDataset('/media/feyre/DATA1/Datasets/SceneNetRGBD/pySceneNetRGBD/data/train',
						megabatch_size=50000, major_seed=5, minor_seed=1)

	for mbatch_i, dataset_batch in enumerate(dataset):

		print('\nmajor batch: {}'.format(mbatch_i))

		dataloader = torch.utils.data.DataLoader(
			dataset_batch,
			batch_size=8,
			shuffle=False,
			num_workers=0,
			pin_memory=True,
			collate_fn=dataset_batch.collate_fn,
		)
		print('.')

		for batch_i, (image_paths, images, targets) in enumerate(dataloader):


			if (batch_i < 10):
				print(images.shape)

			else:
				break


			# for image_i, image in enumerate(images):
			#
			# 	tensor_image = (image.numpy()*255.0).astype('uint8').transpose((1,2,0))
			# 	_targets = targets[targets[:,0].int()==image_i, 1:]
			#
			# 	for target in _targets:
			# 		pil_image = Image.fromarray(tensor_image, 'RGB')
			#
			# 		nbox = target[1:].numpy()
			# 		box = dataset_batch.denormalize_box(nbox, tensor_image.shape[0], tensor_image.shape[0]) #NOTE: dunno w/h
			# 		print(NYU_13_CLASSES[int(target[0])], box)
			#
			# 		draw = ImageDraw.Draw(pil_image)
			# 		draw.rectangle(box, outline=(255,0,0))
			# 		del draw
			# 		plt.imshow(pil_image)
			# 		plt.show()
