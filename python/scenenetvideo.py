# Copyright (c) 2018, Lachlan Nicholson, ARC Centre of Excellence for Robotic Vision, Queensland University of Technology (QUT)
# All rights reserved.
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Created by Lachlan on 12/12/18.
#

# import standard libraries
from __future__ import print_function
import numpy as np
import sys, os
from PIL import Image
from scipy import misc
from skimage.transform import resize
import random
import importlib
import time as timelib
import json
import errno

from PIL import ImageDraw
import torch
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms as transforms
import torch.nn.functional as F

import re
# import cv2



WNID_TO_NYU = {
	'04593077':4, '03262932':4, '02933112':6, '03207941':7, '03063968':10, '04398044':7, '04515003':7,
	'00017222':7, '02964075':10, '03246933':10, '03904060':10, '03018349':6, '03786621':4, '04225987':7,
	'04284002':7, '03211117':11, '02920259':1, '03782190':11, '03761084':7, '03710193':7, '03367059':7,
	'02747177':7, '03063599':7, '04599124':7, '20000036':10, '03085219':7, '04255586':7, '03165096':1,
	'03938244':1, '14845743':7, '03609235':7, '03238586':10, '03797390':7, '04152829':11, '04553920':7,
	'04608329':10, '20000016':4, '02883344':7, '04590933':4, '04466871':7, '03168217':4, '03490884':7,
	'04569063':7, '03071021':7, '03221720':12, '03309808':7, '04380533':7, '02839910':7, '03179701':10,
	'02823510':7, '03376595':4, '03891251':4, '03438257':7, '02686379':7, '03488438':7, '04118021':5,
	'03513137':7, '04315948':7, '03092883':10, '15101854':6, '03982430':10, '02920083':1, '02990373':3,
	'03346455':12, '03452594':7, '03612814':7, '06415419':7, '03025755':7, '02777927':12, '04546855':12,
	'20000040':10, '20000041':10, '04533802':7, '04459362':7, '04177755':9, '03206908':7, '20000021':4,
	'03624134':7, '04186051':7, '04152593':11, '03643737':7, '02676566':7, '02789487':6, '03237340':6,
	'04502670':7, '04208936':7, '20000024':4, '04401088':7, '04372370':12, '20000025':4, '03956922':7,
	'04379243':10, '04447028':7, '03147509':7, '03640988':7, '03916031':7, '03906997':7, '04190052':6,
	'02828884':4, '03962852':1, '03665366':7, '02881193':7, '03920867':4, '03773035':12, '03046257':12,
	'04516116':7, '00266645':7, '03665924':7, '03261776':7, '03991062':7, '03908831':7, '03759954':7,
	'04164868':7, '04004475':7, '03642806':7, '04589593':13, '04522168':7, '04446276':7, '08647616':4,
	'02808440':7, '08266235':10, '03467517':7, '04256520':9, '04337974':7, '03990474':7, '03116530':6,
	'03649674':4, '04349401':7, '01091234':7, '15075141':7, '20000028':9, '02960903':7, '04254009':7,
	'20000018':4, '20000020':4, '03676759':11, '20000022':4, '20000023':4, '02946921':7, '03957315':7,
	'20000026':4, '20000027':4, '04381587':10, '04101232':7, '03691459':7, '03273913':7, '02843684':7,
	'04183516':7, '04587648':13, '02815950':3, '03653583':6, '03525454':7, '03405725':6, '03636248':7,
	'03211616':11, '04177820':4, '04099969':4, '03928116':7, '04586225':7, '02738535':4, '20000039':10,
	'20000038':10, '04476259':7, '04009801':11, '03909406':12, '03002711':7, '03085602':11, '03233905':6,
	'20000037':10, '02801938':7, '03899768':7, '04343346':7, '03603722':7, '03593526':7, '02954340':7,
	'02694662':7, '04209613':7, '02951358':7, '03115762':9, '04038727':6, '03005285':7, '04559451':7,
	'03775636':7, '03620967':10, '02773838':7, '20000008':6, '04526964':7, '06508816':7, '20000009':6,
	'03379051':7, '04062428':7, '04074963':7, '04047401':7, '03881893':13, '03959485':7, '03391301':7,
	'03151077':12, '04590263':13, '20000006':1, '03148324':6, '20000004':1, '04453156':7, '02840245':2,
	'04591713':7, '03050864':7, '03727837':5, '06277280':11, '03365592':5, '03876519':8, '03179910':7,
	'06709442':7, '03482252':7, '04223580':7, '02880940':7, '04554684':7, '20000030':9, '03085013':7,
	'03169390':7, '04192858':7, '20000029':9, '04331277':4, '03452741':7, '03485997':7, '20000007':1,
	'02942699':7, '03231368':10, '03337140':7, '03001627':4, '20000011':6, '20000010':6, '20000013':6,
	'04603729':10, '20000015':4, '04548280':12, '06410904':2, '04398951':10, '03693474':9, '04330267':7,
	'03015149':9, '04460038':7, '03128519':7, '04306847':7, '03677231':7, '02871439':6, '04550184':6,
	'14974264':7, '04344873':9, '03636649':7, '20000012':6, '02876657':7, '03325088':7, '04253437':7,
	'02992529':7, '03222722':12, '04373704':4, '02851099':13, '04061681':10, '04529681':7,}
NYU_13_CLASSES = ['Bed',
				  'Books',
				  'Ceiling',
				  'Chair',
				  'Floor',
				  'Furniture',
				  'Objects',
				  'Picture',
				  'Sofa',
				  'Table',
				  'TV',
				  'Wall',
				  'Window']

def natural_sort(l):
	convert = lambda text: int(text) if text.isdigit() else text.lower()
	alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
	return sorted(l, key = alphanum_key)

class SceneNetDataset(torch.utils.data.Dataset):
	def __init__(self, root_dir):
		'''
			Loads video paths.
			Incrementally loads protobuf files as required.
		'''

		# load protobuf definition
		self.sn = self.load_protobuf_reader('/media/feyre/DATA1/Datasets/SceneNetRGBD/pySceneNetRGBD/scenenet_pb2.py')

		# get protobuf folder from dataset name
		dataset_name = root_dir.split('/')[-1] if (not root_dir.endswith('/')) else root_dir.split('/')[-2]
		protobuf_folder = os.path.join(os.path.realpath(os.path.join(root_dir, '../')), '{}_protobufs'.format(dataset_name))

		# collect video paths
		render_folders = [fn for fn in os.listdir(root_dir)
								if os.path.isdir(os.path.join(root_dir, fn))]

		video_paths = [os.path.join(root_dir, pf, fn) for pf in render_folders
									for fn in os.listdir(os.path.join(root_dir, pf))
										if (not '.' in fn)]

		self.root_dir = root_dir
		self.dataset_name = dataset_name
		self.protobuf_folder = protobuf_folder
		self.video_paths = video_paths
		self.video_to_info = {}

	def __len__(self):
		return len(self.video_paths)

	def __getitem__(self, index):
		# get video path
		video_path = self.video_paths[index]

		# get instance to NYU13
		if (not video_path in self.video_to_info):
			render_folder = video_path.split('/')[-2]
			protobuf_name = 'scenenet_rgbd_{}_{}.pb'.format(self.dataset_name, render_folder)
			self.load_protobuf(protobuf_name)
		instance_to_nyu = self.video_to_info[video_path]

		# return video dataset
		return SceneNetVideo(video_path, instance_to_nyu)


	def load_protobuf(self, protobuf_name):
		trajectories = self.sn.Trajectories()
		with open(os.path.join(self.protobuf_folder, protobuf_name),'rb') as f:
			trajectories.ParseFromString(f.read())

		for trajectory in trajectories.trajectories:
			render_path = str(trajectory.render_path)
			video_path = os.path.join(self.root_dir, render_path)

			instance_info = {}
			for instance in trajectory.instances:
				if (instance.semantic_wordnet_id != ''):
					instance_info[int(instance.instance_id)] = {'wnid': str(instance.semantic_wordnet_id),
																'name': str(instance.semantic_english),
																'nyu': (WNID_TO_NYU[str(instance.semantic_wordnet_id)] - 1)}
			self.video_to_info[video_path] = instance_info


	def load_protobuf_reader(self, protobuf_def_path='/media/feyre/DATA1/Datasets/SceneNetRGBD/pySceneNetRGBD/scenenet_pb2.py'):
		# extract folder / file information
		protobuf_def_folder = protobuf_def_path.replace(protobuf_def_path.split('/')[-1], '')
		protobuf_def_file = protobuf_def_path.replace(protobuf_def_folder, '')

		# load protobuf definition
		sys.dont_write_bytecode = True
		sys.path.append(os.path.realpath(protobuf_def_folder))
		sn = importlib.import_module(protobuf_def_file.replace('.py',''))
		return sn



class SceneNetVideo(torch.utils.data.Dataset):
	def __init__(self, video_path, instance_to_nyu, image_size=416):
		'''
			Given video path, loads image paths.
		'''

		# load image paths
		image_paths = [os.path.join(video_path, 'photo', fn)
							for fn in os.listdir(os.path.join(video_path, 'photo'))
								if fn.endswith('.jpg')]
		image_paths = natural_sort(image_paths)

		self.image_size = image_size
		self.image_paths = image_paths
		self.instance_to_nyu = instance_to_nyu

	def __len__(self):
		return len(self.image_paths)

	def __getitem__(self, index):
		# load rgb image
		rgb_path = self.image_paths[index]
		image = transforms.ToTensor()(Image.open(rgb_path).convert('RGB'))

		# load instance image
		instance_path = rgb_path.replace('photo','instance').replace('.jpg','.png')
		instance_image = np.array(Image.open(instance_path))

		# calculate instance boxes
		instance_boxes = self.get_boxes(instance_image)

		# convert to labeled boxes
		label_boxes = self.add_labels(instance_boxes)

		# pad image to square
		image, pad = self.pad_to_square(image, 0)
		_, padded_h, padded_w = image.shape

		# adjust boxes for padding
		padded_boxes = self.pad_boxes(label_boxes, pad)

		# resize image
		image = self.resize(image, self.image_size)
		padded_boxes = self.resize_boxes(padded_boxes, padded_w, self.image_size)
		padded_boxes = torch.from_numpy(padded_boxes).float()

		return image, padded_boxes

	def resize_boxes(self, boxes, old_size, new_size):
		_boxes = boxes.astype('float')
		_boxes[:, 2:] *= float(new_size)/old_size
		return _boxes

	def resize(self, image, size):
		image = F.interpolate(image.unsqueeze(0), size=size, mode="nearest").squeeze(0)
		return image

	def pad_boxes(self, boxes, pad):
		if (len(boxes) > 0):
			boxes[:, 2] += pad[0]
			boxes[:, 3] += pad[2]
			boxes[:, 4] += pad[1]
			boxes[:, 5] += pad[3]
		return boxes

	def pad_to_square(self, image, pad_value):
		c, h, w = image.shape
		dim_diff = np.abs(h - w)
		pad1, pad2 = dim_diff // 2, dim_diff - dim_diff // 2
		pad = (0, 0, pad1, pad2) if h <= w else (pad1, pad2, 0, 0)
		image = F.pad(image, pad, "constant", value=pad_value)
		return image, pad

	def add_labels(self, instance_boxes):
		'''
			Converts an instance image into suitable class boxes
			Returns linear structure [instance, label, x1, y1, x2, y2] (n_instances, 5):
			Requires: self.instance_to_nyu
		'''
		label_boxes = []
		for instance_box in instance_boxes:
			if (instance_box[0] in self.instance_to_nyu):
				label = self.instance_to_nyu[instance_box[0]]['nyu']
				label_box = [instance_box[0], label] + instance_box[1:].tolist()
				label_boxes.append(label_box)

		label_boxes = np.array(label_boxes)
		return label_boxes

	def get_boxes(self, instance_image):
		'''
			Converts an instance image into suitable class boxes
			Returns linear structure [instance_id, x1, y1, x2, y2] (n_instances, 5):
		'''
		boxes = []
		for instance_id in np.unique(instance_image):

			# get masks
			instance_mask = (instance_image == instance_id)
			horizontal_mask = np.any(instance_mask, axis=0)
			vertical_mask = np.any(instance_mask, axis=1)

			# calculate bounds
			xmin, xmax = np.where(horizontal_mask)[0][[0,-1]]
			ymin, ymax = np.where(vertical_mask)[0][[0,-1]]

			# normalize box coordinates
			box = [instance_id, xmin, ymin, xmax, ymax]
			boxes.append(box)
		return np.array(boxes)




if __name__ == '__main__':

	import matplotlib.pyplot as plt
	import matplotlib.patches as patches

	dataset = SceneNetDataset('/media/feyre/DATA1/Datasets/SceneNetRGBD/pySceneNetRGBD/data/train')

	for video in dataset:
		for (image, targets) in video:
			cvimage = image.permute(1,2,0).numpy()[...,::-1]

			for target in targets:
				print('[test] instance ({}) label ({}) class ({})'.format(int(target[0]), int(target[1]), NYU_13_CLASSES[int(target[1])]))

				fig, ax = plt.subplots(1)
				ax.clear()
				ax.imshow(cvimage)
				rect = patches.Rectangle((target[2], target[3]), target[4]-target[2], target[5]-target[3], edgecolor='r', facecolor='none')
				ax.add_patch(rect)
				plt.show()

				# imcopy = cvimage.copy()
				# cv2.rectangle(imcopy, (int(target[2]),int(target[3])), (int(target[4]),int(target[5])), (0,0,255), 2)
				# cv2.imshow('rgb', imcopy)
				# cv2.waitKey(0)
