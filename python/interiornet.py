# Copyright (c) 2018, Lachlan Nicholson, ARC Centre of Excellence for Robotic Vision, Queensland University of Technology (QUT)
# All rights reserved.
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Created by Lachlan on 12/12/18.
#

# import standard libraries
from __future__ import print_function
import numpy as np
import sys, os
from PIL import Image
from scipy import misc
from skimage.transform import resize
import random
import importlib
import time as timelib
import json
import errno

from PIL import ImageDraw
import torch
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms as transforms
import torch.nn.functional as F
from collections import defaultdict

# object detector training:
# - image_paths
# - annotations_paths
# video evaluation:
# - boxes (w/ instance and class)

# dataset contains (video_paths)
# video contains (image_paths, image_annotations)


# for video in dataset:
# 	for image_fn, image_annotations in video:
		# for detection in image_detections:
		# 	associate(detection, image_annotations)











class InteriorNetDataset(torch.utils.data.Dataset):
	def __init__(self, root_dir, verbose=True):
		'''
		Loads video paths from dataset folder
		Provides __getitem__ (VideoDataset(video_path))
		'''

		t0 = timelib.time()
		if (verbose):
			print('[dataset] Loading image filepaths.')

		# load dataset filenames
		with open(os.path.join(root_dir, 'InteriorNetFiles.txt')) as f:
			lines = f.readlines()

		# remove extensions and endlines
		scene_paths = [line.replace('\n','').split('.')[0] for line in lines]

		# checkpoint
		t1 = timelib.time()
		if (verbose):
			print('[dataset] Loaded scenes list: {:.3f}s'.format(t1-t0))



		# get only existing scenes (check all if valid, or gather all and check if in)
		# scene_paths = [scene_path for scene_path in scene_paths if os.path.exists(os.path.join(root_dir, scene_path))]
		primary_folders = [pf for pf in os.listdir(root_dir)
								if os.path.isdir(os.path.join(root_dir, pf))]
		secondary_folders = [sf for pf in primary_folders
									for sf in os.listdir(os.path.join(root_dir, pf))
										if (not '.' in sf)]
		scene_paths = [scene_path for scene_path in scene_paths if scene_path.split('/')[-1] in secondary_folders]

		# checkpoint
		t2 = timelib.time()
		if (verbose):
			print('[dataset] Remove nonexisting scenes only: {:.3f}s'.format(t2-t1))


		# DEBUG:
		scene_paths = [sp for sp in scene_paths if ('3FO4KG6E7BUM' in sp)]

		# for each scene get all sequence folders
		video_paths = [os.path.join(root_dir, scene_path, folder_name)
							for scene_path in scene_paths
								for folder_name in os.listdir(os.path.join(root_dir, scene_path))
								 	if '.' not in os.path.join(root_dir, scene_path, folder_name)]

		# checkpoint
		t3 = timelib.time()
		if (verbose):
			print('[dataset] Collected video paths for each scene: {:.3f}s'.format(t3-t2))

		if (verbose):
			print('[dataset] Dataset load time: {:.3f}s'.format(t3-t0))
			print('[dataset] Loaded {} scene paths, {} video paths'.format(len(scene_paths), len(video_paths)))

		self.video_paths = video_paths


	def __getitem__(self, index):
		'''
		Returns VideoDataset(video_path)
		'''
		video_path = self.video_paths[index % self.__len__()]
		video = ImageNetVideoDataset(video_path)
		return video

	def __len__(self):
		return len(self.video_paths)



# video_info = {image_data['id']: (os.path.join(video_path, image_data['file_name']), [])
# 									for image_data in cocolabel['images']}
#
# # associate annotations to image_ids
# # WARNING: will fail if image id not keyed
# for annotation_i, annotation in enumerate(cocolabel['annotations']):
# 	video_info[annotation['image_id']][1].append(annotation)


class ImageNetVideoDataset(torch.utils.data.Dataset):
	def __init__(self, video_path, verbose=True):
		'''
		Loads image_paths, instance_paths
		'''
		t0 = timelib.time()
		self.image_paths = []
		self.instance_paths = []

		# load video data file
		cocolabel = json.load(open(os.path.join(video_path, 'cocolabel.json')))

		# load class names
		classes = {int(c['id']): str(c['name']) for c in cocolabel['categories']}
		self.class_names = [classes[id] for id in sorted(classes.keys())]

		# assume data is not-ordered by ID
		video_info = defaultdict(dict)
		for image_data, instance_data in zip(cocolabel['images'], cocolabel['maskimages']):
			video_info[image_data['id']]['image_path'] = image_data['file_name']
			video_info[image_data['id']]['instance_path'] = instance_data['mask_image_filename']

		# convert to sorted list
		for key in sorted(video_info.keys()):
			if (not 'image_path' in video_info[key]):
				print('[video] ID({}) has no image'.format(key))
				continue
			if (not 'instance_path' in video_info[key]):
				print('[video] ID({}) has no instance image'.format(key))
				continue
			self.image_paths.append(os.path.join(video_path, video_info[key]['image_path']))
			self.instance_paths.append(os.path.join(video_path, video_info[key]['instance_path']))

		if (verbose):
			print('[video] video loading took {:.3f}s'.format(timelib.time()-t0))


	def __getitem__(self, index):
		'''
		Loads image, calculates targets from annotations.
		Returns (img, targets) [label, instance, bbox]
		'''
		t0 = timelib.time()
		image_path = self.image_paths[index % len(self)]
		instance_path = self.instance_paths[index % len(self)]

		# load image
		# cvimage = cv2.imread(image_path)
		rgb_image = transforms.ToTensor()(Image.open(image_path).convert('RGB'))
		instance_image = np.array(Image.open(instance_path))
		label_image = np.array(Image.open(instance_path.replace('instance.', 'nyu.')))
		# _, height, width = rgb_image.shape

		# calculate instance boxes
		t1 = timelib.time()
		targets = self.get_targets(instance_image, label_image)
		t2 = timelib.time()

		# ignore boxes labelled zero
		targets = torch.FloatTensor([target for target in targets if (target[1] != 0)])

		# zero center labels
		targets[:, 1] -= 1

		print('[video] image retrival took {:.3f}s | calcs took {:.3f}s'.format(timelib.time()-t0, t2-t1))
		return ([rgb_image, instance_image, label_image], targets)


	def get_targets(self, instance_image, label_image):
		'''
			Converts an instance image into suitable class boxes
			Returns linear structure [instance, nyu40, xmin, ymin, xmax, ymax] (n_instances, 6):
		'''
		instance_ids = np.unique(instance_image)

		targets = []
		for i, instance_id in enumerate(instance_ids):

			# get masks
			instance_mask = (instance_image == instance_id)
			horizontal_mask = np.any(instance_mask, axis=0)
			vertical_mask = np.any(instance_mask, axis=1)

			# calculate bounds
			xmin, xmax = np.where(horizontal_mask)[0][[0,-1]]
			ymin, ymax = np.where(vertical_mask)[0][[0,-1]]

			# get a point and convert to label
			ys, xs = np.where(instance_mask)
			label = label_image[ys[0], xs[0]]

			# normalize box coordinates
			target = [instance_id, label, xmin, ymin, xmax, ymax]
			targets.append(target)
		return targets


	def normalize_box(self, box, width, height):
		'''
		Converts [xmin,ymin,xmax,ymax] -> [%cx,%cy,%w,%h]
		'''
		w = box[2]-box[0]
		h = box[3]-box[1]
		nbox = [float(box[0]+(w)/2.0) / width,
				float(box[1]+(h)/2.0) / height,
				float(w) / width,
				float(h) / height]
		return nbox

	def denormalize_box(self, nbox, width, height):
		'''
		Converts [%cx,%cy,%w,%h] -> [xmin, ymin, xmax, ymax]
		'''
		box = [int(round((nbox[0]-(nbox[2])/2.0) * width)),
			   int(round((nbox[1]-(nbox[3])/2.0) * height)),
			   int(round((nbox[0]+(nbox[2])/2.0) * width)),
			   int(round((nbox[1]+(nbox[3])/2.0) * height))]
		return box

	def __len__(self):
		return len(self.image_paths)


images = None
def onmouse(event, x, y, flags, param):
	global images
	if event == cv2.EVENT_LBUTTONDOWN:
		print('Instance:{} | Label:{}'.format(images[1][y,x], images[2][y,x]))

if __name__ == "__main__":

	import cv2
	dataset = InteriorNetDataset('/media/feyre/DATA1/Datasets/InteriorNetFullResolution')

	for video in dataset:
		for ((rgb_image, instance_image, label_image), targets) in video:
			cv_rgb_image = rgb_image.permute(1,2,0).numpy()[...,::-1]
			cv_instance_image = instance_image.astype('float')/255.0
			cv_label_image = label_image.astype('float')/255.0

			for target in targets.numpy():
				imcopy = cv_rgb_image.copy()
				print('[test] instance ({}) label ({}) class ({})'.format(
					int(target[0]), int(target[1]), video.class_names[int(target[1])])
				)
				cv2.rectangle(imcopy, (int(target[2]),int(target[3])), (int(target[4]),int(target[5])), (0,0,255), 2)
				cv2.imshow('rgb', imcopy)
				global images
				images = (rgb_image, instance_image, label_image)
				cv2.setMouseCallback('rgb', onmouse)
				cv2.imshow('instance', cv_instance_image)
				cv2.imshow('label', cv_label_image)
				cv2.waitKey(0)


			# print(image_fn, image_annotations)
			# exit()
			# for detection in image_detections:
			# 	associate(detection, image_annotations)
