import numpy as np

'''
	Bad sensor measurements:
	1. uniform
	3. same wrong class many times
	4. different wrong class each time

	Sigma:
	 - simulated as **2 difference from GT one-hot
'''



def merge(means, vars, method):
	if (method == 'multiply'):
		rolling_product = np.ones(means.shape[1])
		for p in means:
			rolling_product *= p
			rolling_product = rolling_product/rolling_product.sum()
		return rolling_product

	if (method == 'average'):
		return means.mean(0)

	if (method == 'weighted'):
		res = (means/vars).sum(0) / (1.0/vars).sum(0)
		return res

def get_measurements(style):
	means = []
	vars = []
	good_views = [True]*n_good_views + [False]*(n_views-n_good_views)

	# generate good views
	for _ in range(n_good_views):
		means.append(good_mean(n_classes, true_class))

	# generate bad views
	if (style == 'uniform'):
		for _ in range(n_views-n_good_views):
			means.append(np.ones(n_classes)/n_classes)
	if (style == 'wrong_consistent'):
		for _ in range(n_views-n_good_views):
			means.append(good_mean(n_classes, (true_class+1)%n_classes))
	if (style == 'wrong_random'):
		for view_i in range(n_views-n_good_views):
			# i = np.random.randint(1,n_classes) # (1-4)
			i = np.arange(1,n_classes)[view_i%(n_classes-1)]
			means.append(good_mean(n_classes, (true_class+i)%n_classes))


	# generate variances
	for i, mean in enumerate(means):
		# vars.append(np.abs(true_measurement-mean)**2)
		if (i < n_good_views):
			vars.append(np.ones(n_classes)*0.01)
		else:
			vars.append(np.ones(n_classes)*0.1)

	return np.stack(means),np.stack(vars)

def good_mean(n_classes, true_class):
	alpha = np.ones(n_classes)
	alpha[true_class] = 50
	alpha[alpha < 2] = np.random.randint(1,10,n_classes-1)
	return np.random.dirichlet(alpha)







# print options
np.set_printoptions(suppress=True,precision=3)

# meta settings
n_classes = 5
true_class = 3
true_measurement = np.zeros(n_classes)
true_measurement[true_class] = 1.0


# test settings
n_views = 10
n_good_views = 2
styles = ['uniform', 'wrong_consistent', 'wrong_random']
methods = ['multiply', 'average', 'weighted']


print('True Measurement: ', true_measurement)

for style in styles:
	print('\nStyle:{}'.format(style))
	ms,vs = get_measurements(style)
	# print('means:'); print(ms)

	for method in methods:
		res = merge(ms,vs, method=method)
		print('{name:<10} | PGT {pgt:7.3f} | successful? {cor}'.format(name=method, pgt=res[true_class], cor=res.argmax()==true_class))
		# print(method, res)
