# Copyright (c) 2018, Lachlan Nicholson, ARC Centre of Excellence for Robotic Vision, Queensland University of Technology (QUT)
# All rights reserved.
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Created by Lachlan on 12/12/18.
#

# import standard libraries
from __future__ import print_function
import time as timelib
import sys, os
import colorsys
# import cv2

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision import transforms
from torch.autograd import Variable
import torch.optim as optim

from abc import ABCMeta, abstractmethod
from collections import defaultdict

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pickle

# import shared libraries
sys.dont_write_bytecode = True
sys.path.insert(0, '/home/feyre/git_ws/object_detectors/pytorch-yolov3-eriklin')
from trainensemble import EnsembleDetector
from models import *
from utils.utils import *

# import files
from scenenetvideo import SceneNetDataset, SceneNetVideo
from scenenetvideo import NYU_13_CLASSES


### OBJECT DETECTOR

def load_model():
	model_def = '/home/feyre/git_ws/object_detectors/pytorch-yolov3-eriklin/config/yolov3_13_dropout.cfg'
	# model_def = '/home/feyre/git_ws/object_detectors/pytorch-yolov3-eriklin/config/yolov3_13.cfg'
	weights_path = '/home/feyre/git_ws/object_detectors/pytorch-yolov3-eriklin/checkpoints/yolov3_ckpt_do_i950000.pth'
	# weights_path = '/home/feyre/git_ws/object_detectors/pytorch-yolov3-eriklin/checkpoints/first_attempt/yolov3_ckpt_9.pth'
	device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
	model = Darknet(model_def).to(device)
	if weights_path.endswith(".weights"):
		model.load_darknet_weights(weights_path)
	else:
		model.load_state_dict(torch.load(weights_path))
	return model

def bayes_pass(model, image, samples, scores=False):
	torch.cuda.empty_cache()
	model.eval()
	model.dropout_mode(on=True)
	Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor
	input = image.unsqueeze(0).repeat(samples,1,1,1)
	input = input.type(Tensor)
	with torch.no_grad():
		outputs = model(input, scores=scores)

	# outputs is shape (dropout, anchors, 18)
	output_boxes = outputs[:,:,0:4]
	output_boxes = output_boxes.unsqueeze(3).permute(1,0,2,3)

	# output_boxes of shape (10647, 20, 4, 1)
	# mean of shape (10647, 4, 1)
	# cov of shape (10647, 4, 4)

	# test single
	prior_box = model.get_anchors(input)[:,0,:]
	prior_mean = prior_box.mean(0).unsqueeze(1)
	prior_cov = torch.eye(4) * 10000
	print('prior_mean', prior_mean)
	print('prior_cov', prior_cov)

	output_boxes = output_boxes[0]
	likelyhood_mean = output_boxes.mean(0)
	likelyhood_cov = (output_boxes.matmul(output_boxes.transpose(1,2)).sum(0)).div(samples).sub(likelyhood_mean.matmul(likelyhood_mean.t()))
	# likelyhood_cov = torch.diag(torch.diag(likelyhood_cov))
	# likelyhood_cov = torch.eye(4) * 1
	print('likelyhood_mean', likelyhood_mean)
	print('likelyhood_cov', likelyhood_cov)

	posterior_cov = (prior_cov.inverse().add(likelyhood_cov.inverse())).inverse()
	posterior_mean = posterior_cov.matmul(prior_cov.inverse().matmul(prior_mean).add(likelyhood_cov.matmul(likelyhood_mean)))
	print('posterior_mean', posterior_mean)
	print('posterior_cov', posterior_cov)

	# show_boxes(image, prior_mean.unsqueeze(0))
	# show_boxes(image, likelyhood_mean.unsqueeze(0))
	show_boxes(image, posterior_mean.unsqueeze(0))
	# show_boxes(image, output_boxes)


	# prior_cov = torch.eye(4).unsqueeze(0).expand(likelyhood_cov.shape) * 100



	# likelyhood_mean = output_boxes.mean(1)
	# likelyhood_cov = ((output_boxes.matmul(output_boxes.transpose(2,3))).sum(1)).div(output_boxes.size(1)).sub((likelyhood_mean.matmul(likelyhood_mean.transpose(1,2))))
	# likelyhood_cov = torch.eye(4).unsqueeze(0).expand(likelyhood_cov.shape) * 100
	#
	# prior_mean = model.get_anchors(input).permute(1,0,2).unsqueeze(3).mean(1)
	# prior_cov = torch.eye(4).unsqueeze(0).expand(likelyhood_cov.shape) * 100
	#
	# posterior_cov = (prior_cov.inverse() + likelyhood_cov.inverse()).inverse()
	# posterior_mean = posterior_cov.matmul(prior_cov.inverse().matmul(prior_mean) + likelyhood_cov.matmul(likelyhood_mean))
	#
	# # clustering
	# objectness = outputs[:,:,4].mean(0)
	# anchor_index = torch.arange(len(objectness)).unsqueeze(1).float()
	# posterior = torch.cat((posterior_mean.squeeze(2), anchor_index), 1) # (10647, 5)
	#
	# sorted_boxes = posterior[(-objectness).argsort()]
	# show_boxes(image, posterior_mean)

	# clusters = []
	# while sorted_boxes.size(0):
	# 	print(sorted_boxes.size(0))
	# 	large_overlap = bbox_iou(sorted_boxes[0, :4].unsqueeze(0), sorted_boxes[:, :4]) > 0.5
	# 	print(large_overlap.sum())
	# 	clusters += [sorted_boxes[large_overlap, -1].long()]
	# 	sorted_boxes = sorted_boxes[~large_overlap]
	#
	# final_boxes = []
	# for cluster_is in clusters:
	# 	cluster_covs = posterior_cov[cluster_is]
	# 	cluster_means = posterior_mean[cluster_is]
	# 	print(cluster_means.shape)

def sample_pass(model, image, samples, scores=False):
	model.eval()
	model.dropout_mode(on=True)

	# variables
	nms_thres = 0.5
	conf_thres = 0.5

	# pass image through network
	Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor
	input = image.unsqueeze(0).repeat(samples,1,1,1)
	input = input.type(Tensor)
	with torch.no_grad():
		outputs = model(input, scores=scores)
		outputs = full_non_max_suppression(outputs, conf_thres=conf_thres, nms_thres=nms_thres, scores=scores)
		outputs = [o for o in outputs if (o is not None)]

	if (len(outputs) < 1):
		return Detections()

	# sort detections by max class score
	detections = torch.cat(outputs) # n,18
	if (scores):
		score = torch.sigmoid(detections[:, 4]) * torch.sigmoid(detections[:, 5:]).max(1)[0]
	else:
		score = detections[:, 4] * detections[:, 5:].max(1)[0]
	detections = detections[(-score).argsort()]

	# cluster detections into observations
	observations = []
	while detections.size(0):
		large_overlap = bbox_iou(detections[0, :4].unsqueeze(0), detections[:, :4]) > nms_thres
		observations += [detections[large_overlap, :]]
		detections = detections[~large_overlap]




	# calculate cluster statistics
	# means = []
	# vars = []
	# for cluster in observations:
	# 	cluster_mean = cluster.mean(0)
	# 	cluster_var = cluster.var(0)
	# 	if (torch.any(torch.isnan(cluster_mean)) or torch.any(torch.isnan(cluster_var))):
	# 		continue
	# 	if (torch.any(cluster_var == 0.0)):
	# 		continue
	# 	means.append(cluster_mean)
	# 	vars.append(cluster_var)
	# if (len(means) > 0):
	# 	outputs = torch.stack((torch.stack(means), torch.stack(vars)),1)
	# else:
	# 	outputs = []

	# return clusters
	list_detections = []
	for cluster in observations:
		n_detections = cluster.shape[0]
		n_unique = int(cluster.unique(dim=0).shape[0])
		if (n_unique > 5):
			sampled_detection = SampledDetection(cluster[:,0:4], cluster[:,4], cluster[:,5:])
			list_detections.append(sampled_detection)
	detections = Detections(list_detections)

	return detections

def single_pass(model, image, scores=False):
	model.eval()
	Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor
	input = image.unsqueeze(0)
	input = Variable(input.type(Tensor), requires_grad=False)
	with torch.no_grad():
		outputs = model(input, scores=scores) # (batch, detection, 18) [box,obj,scores] [x,y,w,h]
		# outputs = non_max_suppression(outputs, conf_thres=0.5, nms_thres=0.5)
		outputs = full_non_max_suppression(outputs, conf_thres=0.2, nms_thres=0.2, scores=scores) # (img, detection, 7) [x1, y1, x2, y2, object_conf, class_score, class_pred]
	outputs = outputs[0]
	print('[single] detected {} objects'.format(len(outputs)))
	return outputs


# def associate(instances, detections, iou_thres=0.2):
# 	'''
# 		detections (n, s, 18)
# 	'''
#
# 	# associate each detection with a target
# 	associations = []
# 	for detection in detections:
# 		mean_box = detection.mean(0)
# 		ious = bbox_iou(mean_box.unsqueeze(0), instances[:, 2:6])
# 		max_iou, max_index = ious.max(0, keepdim=True)
# 		if (iou_thres > 0.2):
# 			associations.append([instances[max_index, 0].item(), detection])
# 	return associations











class MergingMethods(object):
	def __init__(self):
		pass

	def merge2(self, object_detections):
		scores = torch.stack([d.distribution_mean for d in object_detections])
		sigmoids = torch.stack([torch.sigmoid(d.distribution_mean) for d in object_detections])
		probabilities = torch.stack([torch.nn.functional.softmax(d.distribution_mean, dim=0) for d in object_detections])
		scores_var = torch.stack([torch.diag(d.distribution_cov) for d in object_detections])

		results = {}
		results['multiply_probs'] = self.multiply(probabilities)
		results['average_scores'] = self.average(scores)
		results['average_sigmoids'] = self.average(sigmoids)
		results['average_probabilities'] = self.average(probabilities)
		results['max_scores'] = self.max(scores)
		results['max_sigmoids'] = self.max(sigmoids)
		results['max_probabilities'] = self.max(probabilities)
		results['weighted_scores'] = self.weighted_average(scores, scores_var)
		results['sample_dirichlet_10'] = self.sampling_dirichlet(probabilities, samples=10)
		results['sample_dirichlet_100'] = self.sampling_dirichlet(probabilities, samples=100)
		names = results.keys()
		values = torch.stack(list(results.values()))
		return names, values


	def merge(self, object_detections):
		# get sensor means
		# samples = torch.stack([ torch.FloatTensor([detection.distributions.shape[0]]*detection.distributions.shape[1]) for detection in object_detections])
		scores = torch.stack([ detection.distributions.mean(0) 												for detection in object_detections])
		sigmoids = torch.stack([ torch.sigmoid(detection.distributions).mean(0) 							for detection in object_detections])
		probabilities = torch.stack([ torch.nn.functional.softmax(detection.distributions, dim=1).mean(0) 	for detection in object_detections])

		# scores2 = torch.stack([ detection.distributions.mean(0) 												for detection in object_detections])
		# sigmoids2 = torch.stack([ torch.sigmoid(detection.distributions.mean(0)) 							for detection in object_detections])
		# probabilities2 = torch.stack([ torch.nn.functional.softmax(detection.distributions.mean(0), dim=0) 	for detection in object_detections])

		# get sensor variance
		scores_var = torch.stack([ detection.distributions.var(0) 												for detection in object_detections])
		sigmoids_var = torch.stack([ torch.sigmoid(detection.distributions).var(0)	 							for detection in object_detections])
		probabilities_var = torch.stack([ torch.nn.functional.softmax(detection.distributions, dim=1).var(0) 	for detection in object_detections])
		scores_var.clamp_(1e-4, 1000)
		sigmoids_var.clamp_(1e-4, 1000)
		probabilities_var.clamp_(1e-4, 1000)

		# sum_var = torch.stack([ detection.distributions.var(0).sum().repeat(detection.distributions.shape[1]) for detection in object_detections])


		results = {}
		results['multiply_probs'] 				= self.multiply(probabilities)
		results['average_scores'] 				= torch.nn.functional.softmax(self.average(scores),0)
		results['average_sigmoids'] 			= self.sigmoid_to_softmax(self.average(sigmoids))
		results['average_probabilities'] 		= self.average(probabilities)
		results['weighted_scores'] 				= torch.nn.functional.softmax(self.weighted_average(scores, scores_var), 0)
		results['weighted_sigmoids'] 			= self.sigmoid_to_softmax(self.weighted_average(sigmoids, sigmoids_var))
		results['weighted_probabilities'] 		= self.normalize(self.weighted_average(probabilities, probabilities_var))
		results['sample_dirichlet_10'] 			= self.sampling_dirichlet(probabilities, samples=10)
		results['sample_dirichlet_100'] 		= self.sampling_dirichlet(probabilities, samples=100)

		# results['max_scores'] 				= torch.nn.functional.softmax(self.max(scores), 0)
		# results['max_sigmoids'] 				= self.sigmoid_to_softmax(self.max(sigmoids))
		# results['max_probabilities'] 			= self.max(probabilities)
		# print(results['weighted_probabilities'],'\n')
		# print(probabilities_var.min(), probabilities_var.max())


		# results['weighted_sum_var'] 			= torch.nn.functional.softmax(self.weighted_average(scores, sum_var), 0)
		# results['combine_normal_scores'] 		= torch.nn.functional.softmax(self.bayesian_product_of_products(scores, scores_var, samples), 0)
		# results['combine_normal_sigmoids'] 		= self.sigmoid_to_softmax(self.bayesian_product_of_products(sigmoids, sigmoids_var, samples))
		# results['combine_normal_probabilities'] = self.bayesian_product_of_products(probabilities, probabilities_var, samples); results['combine_normal_probabilities'] = results['combine_normal_probabilities']/results['combine_normal_probabilities'].sum()
		# results['combine_normal_sum_var'] 		= torch.nn.functional.softmax(self.bayesian_product_of_products(scores, sum_var, samples), 0)
		# results['dirichlet_merge'] = self.dirichlet_merge(object_detections)
		# results['my_merge'] = torch.nn.functional.softmax(self.my_merge(object_detections), 0)


		# results['2_multiply_probs'] = self.multiply(probabilities2)
		# results['2_average_scores'] = self.average(scores2)
		# results['2_average_sigmoids'] = self.average(sigmoids2)
		# results['2_average_probabilities'] = self.average(probabilities2)
		# results['2_max_scores'] = self.max(scores2)
		# results['2_max_sigmoids'] = self.max(sigmoids2)
		# results['2_max_probabilities'] = self.max(probabilities2)
		# results['2_weighted_scores'] = self.weighted_average(scores2, scores_var)
		# results['2_weighted_sigmoids'] = self.weighted_average(sigmoids2, sigmoids_var)
		# results['2_weighted_probabilities'] = self.weighted_average(probabilities2, probabilities_var)
		# results['2_combine_normal_scores'] = self.bayesian_product_of_products(scores2, scores_var, samples)
		# results['2_combine_normal_sigmoids'] = self.bayesian_product_of_products(sigmoids2, sigmoids_var, samples)
		# results['2_combine_normal_probabilities'] = self.bayesian_product_of_products(probabilities2, probabilities_var, samples)
		# results['2_sample_dirichlet_10'] = self.sampling_dirichlet(probabilities2, samples=10)
		# results['2_sample_dirichlet_100'] = self.sampling_dirichlet(probabilities2, samples=100)

		names = list(results.keys())
		values = torch.stack(list(results.values()))
		return names, values

	def normalize(self, vector):
		if (np.isclose(vector.sum().item(), 0)):
			if all((vector >= 0)&(vector <= 1)):
				return vector

		if any(vector < 0):
			vector += torch.abs(vector.min())
		return vector / vector.sum()

	def sigmoid_to_softmax(self, vector):
		return torch.nn.functional.softmax(torch.log(vector/(1-vector)), 0)



	def multiply(self, probability_vectors):
		'''
			Calculates prod(probabilities). Normalizes at each step so it doesn't explode.
		'''
		rolling_product = torch.ones(probability_vectors.shape[1])
		for p in probability_vectors:
			rolling_product *= p
			rolling_product = rolling_product/rolling_product.sum()
		return rolling_product

	def average(self, score_vectors):
		return score_vectors.mean(0)

	def max(self, score_vectors):
		index = np.unravel_index(score_vectors.argmax().item(), score_vectors.shape)
		max_vector = score_vectors[index[0]]
		return max_vector

	def weighted_average(self, score_vectors, variances):
		# score vectors = (n, 13). Var = (n, 13)
		weighted_vector = (score_vectors/variances).sum(0) / (1.0/variances).sum(0)
		return weighted_vector

	def bayesian_product_of_products(self, means, variances, samples):
		estimated_mean = (means*samples/variances).sum(0) / (samples/variances).sum(0)
		return estimated_mean

	def sampling_dirichlet(self, probabilities, samples=10):
		# dirichlet_priors = torch.ones(probabilities.shape) # (n, k)
		categorical_dists = torch.distributions.Categorical(probabilities) # (n)
		categorical_samples = categorical_dists.sample((samples,)).t() # (n x samples)
		dirichlet_likelyhoods = torch.zeros(probabilities.shape) # (n, k)

		# print('probabilities', probabilities)
		# print('dirichlet_likelyhoods', dirichlet_likelyhoods.shape)
		# print('categorical_samples', categorical_samples)

		for det_i, det_samples in enumerate(categorical_samples):
			class_indicies, class_counts = det_samples.unique(return_counts=True)
			# print('class_indicies', class_indicies, class_counts)
			dirichlet_likelyhoods[det_i, class_indicies] = class_counts.float()

		dirichlet_prior = torch.ones(probabilities.shape[1]) # (k)
		dirichlet_posterior = dirichlet_likelyhoods.sum(0) + dirichlet_prior
		categorical_posterior = dirichlet_posterior / dirichlet_posterior.sum()

		# print('dirichlet_likelyhoods', dirichlet_likelyhoods)
		# print('dirichlet_posterior', dirichlet_posterior)
		# print('categorical_posterior', categorical_posterior)
		# print('\n')

		return categorical_posterior

	def dirichlet_merge(self, object_detections):
		probabilities = [torch.nn.functional.softmax(detection.distributions, dim=1).numpy() for detection in object_detections]

		import dirichlet
		from scipy import stats
		# warning, each column must have at least 2 unique values
		# warning, can't have zeros in it

		# import code
		# code.interact(local=locals())

		def normalize(x):
			return (x.T/x.sum(1)).T

		dirichlets = []
		for i, samples in enumerate(probabilities):

			finished = False
			for n in range(10):
				try:
					mle_dir = dirichlet.mle(normalize(samples+np.random.uniform(0,0.01,samples.shape)), tol=1e-3, method='fixedpoint', maxiter=1000)
					dirichlets.append(mle_dir)
					finished = True
				except:
					print('\nFailed {}'.format(n))

				if finished:
					break

			if (not finished):
				print('\nproper failed')
				# import code
				# code.interact(local=locals())

		if (len(dirichlets) <1):
			return torch.from_numpy(np.zeros(13)).float()


		dirichlets = np.array(dirichlets)

		# dirichlets = np.array([dirichlet.mle(samples) for samples in probabilities])
		mean_dirichlet = dirichlets.mean(0)
		mean_cat = torch.from_numpy(stats.dirichlet.mean(mean_dirichlet)).float()
		return mean_cat


	def my_merge(self, object_detections):
		'''
		Each measurement provides a sample x = (x1,...,xk) and a sensor covariance.
		We assume the dropout/ensemble covariance is representative of the sensor noise at that view
		Therefore, we can calculate the most likely vector by assuming each measurement is normally distributed
		'''
		# print(len(object_detections))
		# for ods in object_detections:
		# 	print('  {}'.format(len(ods.distributions.numpy())))
		# exit()

		means = np.stack([ detection.distributions.numpy().mean(0) for detection in object_detections])
		vars = np.stack([ detection.distributions.numpy().var(0) for detection in object_detections])
		covs = np.stack([ np.cov(detection.distributions.numpy().T) for detection in object_detections])


		# if (len(object_detections) > 10):
		# 	import code
		# 	code.interact(local=locals())
		# else:
		# 	res = means.mean(0)

		## PROBABILITY OF GOOD VIEW
		# p_view =

		## LEAST VARIANCE CLASS
		varsort_indices = np.argsort(vars,axis=0)
		sorted_means = np.take_along_axis(means,varsort_indices, axis=0)
		res = sorted_means[0,:]

		## THRESHOLD SIGMA
		# max_var = np.mean(vars)
		# res = np.array([means[vars[:,i]>max_var,i].mean(0) for i in range(means.shape[1])])

		## CLUSTER BASED ON WINNING CLASS
		# argmax_means = np.argmax(means,1)
		# clustered_detections = [means[argmax_means==i] for i in range(means.shape[1])]
		# clustered_vars = np.array([ms.var(0) for ms in clustered_detections]) # C,13
		# clustered_means = np.array([ms.mean(0) for ms in clustered_detections]) # C,13
		# res = (clustered_means/clustered_vars).sum(0) / (1.0/clustered_vars).sum(0)


		## AVERAGE K MOST CERTAIN
		# k = round(len(object_detections)/10.0) # take 10% of measurements
		# k = np.clip(k,1,len(object_detections))
		# varsort_indices = np.argsort(vars,axis=0)
		# sorted_means = np.take_along_axis(means,varsort_indices, axis=0)
		# res = sorted_means[0:int(k), :].mean(0)



		# from scipy.stats import multivariate_normal
		# from scipy.optimize import least_squares



		# def error(est):
		# 	errors = []
		# 	for mean,var,cov in zip(means, vars, covs):
		# 		err = multivariate_normal(est, np.diag(var)).pdf(mean)
		# 		errors.append(err)
		# 	return np.array(errors).sum(0)
		#
		# x0 = means[0]
		# res = least_squares(error, x0).x

		# res = (means/(vars**2)).sum(0) / (1/(vars**2)).sum(0)

		return torch.from_numpy(res).float()











# object_detectors = ['YOLOv3', 'YOLOv3_Dropout', 'YOLOv3_Ensemble', 'YOLOv3_Bayes']
# merging_strategies = ['multiply', 'average_scores', 'weighted_average_scores',
# 						'average_sigmoids', 'weighted_average_sigmoids',
# 						'average_softmaxs', 'weighted_average_softmaxs']



class ObjectDetection(object):
	def __init__(self, box, objectness, scores):
		self.box = box
		self.objectness = objectness
		self.scores = scores

class SampledDetection(object):
	def __init__(self, boxes, objectness_scores, distributions):
		self.boxes = boxes
		self.objectness_scores = objectness_scores
		self.distributions = distributions
		self.instance_key = None

	def mean_box(self):
		return self.boxes.mean(0)
	def mean_scores(self):
		return self.distributions.mean(0)
	def max_class(self):
		return self.mean_scores().argmax()

class ProbabilisticDetection(object):
	def __init__(self, box_mean, box_cov, distribution_mean, distribution_cov):
		self.box_mean = box_mean
		self.box_cov = box_cov
		self.distribution_mean = distribution_mean
		self.distribution_cov = distribution_cov
		self.instance_key = None

	def mean_box(self):
		return self.box_mean
	def mean_scores(self):
		return self.distribution_mean
	def max_class(self):
		return self.mean_scores().argmax()

	# def scores(self):
	# 	return self.mean_scores()
	# def sigmoids(self):
	# 	return torch.sigmoid(self.mean_scores()) #* torch.sigmoid(self.objectness_scores.mean())
	# def probabilities(self):
	# 	return torch.nn.functional.softmax(self.mean_scores(), dim=0)
	# def score_var(self):
	# 	return self.distributions.var(0)
	# def sigmoid_var(self):
	# 	return torch.sigmoid(self.distributions).var(0)
	# def softmax_var(self):
	# 	return torch.nn.functional.softmax(self.distributions, dim=1).var(0)









class Detections(object):
	def __init__(self, list_detections=None):
		if (list_detections is None):
			self.list_detections = []
		else:
			self.list_detections = list_detections

	def add(self, detections):
		if (type(detections) == list):
			self.list_detections += detections
		elif (hasattr(detections, 'mean_box')):
			self.list_detections += [detections]
		elif (hasattr(detections, 'list_detections')):
			self.list_detections += detections.list_detections
		else:
			print('warning, couldnt add detections of type {}'.format(type(detections)))

	def per_instance(self):
		instance_map = defaultdict(Detections)
		for detection in self.list_detections:
			if (detection.instance_key is not None):
				instance_map[detection.instance_key].add(detection)
			else:
				instance_map[-1].add(detection)
		return instance_map

	def at_video(self, video_i):
		return Detections([d for d in self.list_detections if (d.video_i == video_i)])

	def associate(self, instances, iou_thres=0.2):
		# for each detection
		for detection in self.list_detections:
			mean_box = detection.mean_box()

			# calculate overlap with targets
			ious = bbox_iou(mean_box.unsqueeze(0), instances[:, 2:6])
			max_iou, max_index = ious.max(0, keepdim=True)

			# associate if sufficient overlap
			if (max_iou > iou_thres):
				detection.instance_key = int(instances[max_index, 0].item())


	def remove_unassociated(self):
		self.list_detections = [d for d in self.list_detections if (d.instance_key is not None)]

	# def probabilities(self):
	# 	probabilities = [detection.probabilities() for detection in self.list_detections]
	# 	return torch.stack(probabilities)
	# def sigmoids(self):
	# 	sigmoids = [detection.sigmoids() for detection in self.list_detections]
	# 	return torch.stack(sigmoids)
	# def scores(self):
	# 	scores = [detection.scores() for detection in self.list_detections]
	# 	return torch.stack(scores)

	def __len__(self):
		return len(self.list_detections)

	def __getitem__(self, index):
		return self.list_detections[index]


	def remove_bad_objects(self, pgt_thresh, instance_to_nyu):
		bad_objects = 0
		constrained_detections = []
		for instance_id, object_detections in self.per_instance().items():
			nyu_label = instance_to_nyu[instance_id]['nyu']
			probabilities = torch.stack([ torch.nn.functional.softmax(detection.distributions, dim=1).mean(0) 	for detection in object_detections]).numpy()
			pgt = probabilities[:,nyu_label]
			if (pgt.max() >= pgt_thresh):
				constrained_detections += object_detections
			else:
				bad_objects += 1

		self.list_detections = constrained_detections
		return bad_objects



	def set_instances_ratio(self, instance_to_nyu, percentage_correct, range):
		constrained_detections = []
		for instance_id, object_detections in self.per_instance().items():
			label = instance_to_nyu[instance_id]['nyu']
			correct = np.array([True if (o.max_class() == label) else False for o in object_detections])

			n_correct_remove = self.calculate_removal(correct, percentage_correct, range)

			if (correct.sum() < n_correct_remove):
				print(len(correct), correct.sum(), percentage_correct, range)

			# remove correct detections
			if (n_correct_remove > 0):
				remove_indicies = np.random.choice(correct.sum(), n_correct_remove, replace=False)
				correct_to_index = np.where(correct)[0]
				remove_indicies = correct_to_index[remove_indicies]
				object_detections = [o for i,o in enumerate(object_detections) if (i not in remove_indicies)]

			# remove incorrect detections
			if (n_correct_remove < 0):
				remove_indicies = np.random.choice((~correct).sum(), -n_correct_remove, replace=False)
				correct_to_index = np.where(~correct)[0]
				remove_indicies = correct_to_index[remove_indicies]
				object_detections = [o for i,o in enumerate(object_detections) if (i not in remove_indicies)]

			constrained_detections += object_detections
			# print('Current ratio ({:.3f}) with ({}|{}|{}) remove ({})'.format(correct.mean(), len(correct), correct.sum(), (~correct).sum(), n_correct_remove))

		# self.list_detections = constrained_detections
		return Detections(constrained_detections)


	def keep_difficult_instances(self, instance_to_nyu, percentage_correct):
		constrained_detections = []
		for instance_id, object_detections in self.per_instance().items():
			label = instance_to_nyu[instance_id]['nyu']
			correct = np.array([True if (o.max_class() == label) else False for o in object_detections])
			current_percentage = correct.sum() / len(correct)

			# keep only instances that are already more than difficult
			if (current_percentage <= percentage_correct):
				constrained_detections += object_detections

		# self.list_detections = constrained_detections
		return Detections(constrained_detections)


	def eval_uncertainty(self, instance_to_nyu):
		'''
		Uncertainty should be lower for correct measurements
			and higher for incorrect measurements.
		'''

		correct_uncertainties = []
		incorrect_uncertainties = []
		for detection in self.list_detections:
			label = instance_to_nyu[detection.instance_key]['nyu']

			score_var = detection.distributions.var(0)
			sigmoid_var = torch.sigmoid(detection.distributions).var(0)
			prob_var = torch.nn.functional.softmax(detection.distributions, dim=1).var(0)

			winning_label = detection.max_class()
			if (winning_label == label):
				# correct_uncertainties.append(score_var[winning_label])
				correct_uncertainties.append(score_var.sum())
			else:
				# incorrect_uncertainties.append(score_var[winning_label])
				incorrect_uncertainties.append(score_var.sum())

		print('[uncertainty] correct uncertainty', np.mean(correct_uncertainties), np.median(correct_uncertainties), np.std(correct_uncertainties), np.min(correct_uncertainties), np.max(correct_uncertainties))
		print('[uncertainty] incorrect uncertainty', np.mean(incorrect_uncertainties), np.median(incorrect_uncertainties), np.std(incorrect_uncertainties), np.min(incorrect_uncertainties), np.max(incorrect_uncertainties))
		print('')




	def calculate_removal(self, correct, p, r):
		# calculate counts
		nc = float(correct.sum())
		ni = float((~correct).sum())

		# clip percentage correct
		lp = np.clip(p-r, 0.01, 0.99)
		rp = np.clip(p+r, 0.01, 0.99)

		# calculate ratios (nc/ni)
		lr = (lp)/(1-lp)
		rr = (rp)/(1-rp)

		# lower ni
		if (nc < (lr*ni)):
			ni_rem = ni-(nc/lr)
			ni_rem = self.float_ceil(ni_rem)
			return (-ni_rem)

		# lower nc
		if (nc > (rr*ni)):
			nc_rem = nc-(rr*ni)
			nc_rem = self.float_ceil(nc_rem)
			return nc_rem
		return 0

	def float_ceil(self, f):
		if (np.isclose(f, np.floor(f))):
			return int(np.floor(f))
		return int(np.ceil(f))













def draw_everything(image, targets, detections):
	import matplotlib.patches as patches
	from scenenetvideo import NYU_13_CLASSES
	cvimage = image.permute(1,2,0).numpy()


	fig, ax = plt.subplots(1)
	ax.clear()
	ax.imshow(cvimage)
	for target in targets:
		box = target[2:6]
		rect = patches.Rectangle((box[0], box[1]), box[2]-box[0], box[3]-box[1], edgecolor='b', facecolor='none')
		ax.add_patch(rect)
	for detection in detections:
		box = detection.mean_box()
		rect = patches.Rectangle((box[0], box[1]), box[2]-box[0], box[3]-box[1], edgecolor='r', facecolor='none')
		ax.add_patch(rect)
	plt.show()





def inspect_data(video_detections, instance_to_nyu):
	'''
		Plot histograms for each object.
		Check normal and challenging conditions
	'''

	# difficulties = np.linspace(0.0, 1.0, 3, 0.1).tolist()[::-1]
	difficulties = [0.2,0.5,0.8, 1.0][::-1]


	for percentage_correct in difficulties:
		constrained_video_detections = video_detections.keep_difficult_instances(instance_to_nyu, percentage_correct)
		# constrained_video_detections = video_detections.set_instances_ratio(instance_to_nyu, percentage_correct, range=0.05)

		video_detections_per_instance = list(constrained_video_detections.per_instance().items()) # [(id, ods)]
		video_detections_per_instance.sort(key=lambda x:len(x[1]), reverse=True)

		for instance_id, object_detections in video_detections_per_instance:
			nyu_label = instance_to_nyu[instance_id]['nyu']
			name = instance_to_nyu[instance_id]['name']

			correct = np.array([True if (o.max_class() == nyu_label) else False for o in object_detections])
			current_percentage = correct.sum() / len(correct)

			print('\nID: {} | CLASS: {} | DETECTIONS: {} | PERC: {}'.format(instance_id, nyu_label, len(object_detections), current_percentage))


			# investigate distribution of detections
			# samples = torch.stack([ torch.FloatTensor([detection.distributions.shape[0]]*detection.distributions.shape[1]) for detection in object_detections]).numpy()
			# sigmoids = torch.stack([ torch.sigmoid(detection.distributions).mean(0) 							for detection in object_detections]).numpy()
			# probabilities = torch.stack([ torch.nn.functional.softmax(detection.distributions, dim=1).mean(0) 	for detection in object_detections]).numpy()
			# c = np.repeat(np.arange(scores.shape[1])[:,np.newaxis], scores.shape[0], 1).T

			scores = torch.stack([ detection.distributions.mean(0) for detection in object_detections]).numpy()
			classes = np.array(NYU_13_CLASSES)
			ind = np.indices(scores.shape)[1]
			c = classes[ind]
			df = pd.DataFrame(dict(x=scores.flatten(), g=c.flatten()))

			# plot histograms and density functions
			fig = plt.figure()
			for i in range(len(classes)):
				ax = plt.subplot(len(classes),1,i+1)
				plt.hist(scores[:,i], bins=20)
				ax.set_xlim(scores.min()-1, scores.max()+1)
			plt.show()






click_press = [False, 0]
key_press = [False, 0]

def inspect_uncertainty(video_detections, instance_to_nyu, dataset):
	'''
		See how Sigma correlates with measurement correctness
	'''
	global click_press, key_press

	# import code
	# code.interact(local=locals())

	detections_per_object = list(video_detections.per_instance().items())
	print('{} objects'.format(len(detections_per_object)))

	# make figure
	# draw image given object_key and detection_key


	plt.figure()
	det_i = 0
	object_key = 0


	while True:
		instance_id, object_detections = detections_per_object[object_key]
		nyu_label = instance_to_nyu[instance_id]['nyu']
		name = instance_to_nyu[instance_id]['name']
		probabilities = torch.stack([ torch.nn.functional.softmax(detection.distributions, dim=1).mean(0) 	for detection in object_detections]).numpy()
		PGT_avg = probabilities[:,nyu_label].mean()
		print('Object {:<13} | Views {:<5} | aPGT {}'.format(name, len(object_detections), PGT_avg))



		# sigmas = [torch.nn.functional.softmax(detection.distributions, 1).numpy().var(0)[nyu_label] for detection in object_detections]
		sigmas = [detection.distributions.numpy().var(0)[nyu_label] for detection in object_detections]
		pgts = [torch.nn.functional.softmax(detection.distributions, 1).numpy().mean(0)[nyu_label] for detection in object_detections]
		video_is = [detection.video_i for detection in object_detections]
		image_is = [detection.image_i for detection in object_detections]
		boxes = [detection.mean_box().numpy() for detection in object_detections]

		def draw_things(det_i):
			plt.clf()

			# plot image
			plt.subplot(1,2,1)
			image, targets = dataset[video_is[det_i]][image_is[det_i]]
			cvimage = image.permute(1,2,0).numpy()
			plt.imshow(cvimage)
			box = boxes[det_i]
			rect = patches.Rectangle((box[0], box[1]), box[2]-box[0], box[3]-box[1], edgecolor='b', facecolor='none')
			ax = plt.gca()
			ax.add_patch(rect)

			# plot data
			plt.subplot(1,2,2)
			plt.scatter(sigmas, pgts, marker='x')
			plt.scatter(sigmas[det_i], pgts[det_i], marker='x', c='r')
			plt.ylim(0,1)

		det_i = 0
		draw_things(det_i)

		def onclick(event):
			global click_press
			click_press = [True, event.xdata, event.ydata]

			# global det_i, object_key, update_object, update_view


		def onkey(event):
			global key_press
			key_press = [True, event.key]

			# print('key event ',event.key)
			# global det_i, object_key, update_object, update_view


		fig = plt.gcf()
		f1 = fig.canvas.mpl_connect('button_press_event', onclick)
		f2 = fig.canvas.mpl_connect('key_press_event', onkey)

		update_object = False
		update_view = False

		while True:
			plt.draw()
			plt.pause(0.01)

			if (key_press[0]):
				key_press[0] = False
				if (key_press[1] == 'n'):
					object_key += 1
					object_key = np.clip(object_key, 0, len(detections_per_object)-1)
					update_object = True
					# print('change obj_key', object_key)
				if (key_press[1] == 'b'):
					object_key -= 1
					object_key = np.clip(object_key, 0, len(detections_per_object)-1)
					update_object = True
					# print('change obj_key', object_key)
				if (key_press[1] == 'h'):
					det_i += 1
					det_i = np.clip(det_i, 0, len(object_detections)-1)
					update_view = True
					# print('change det_i', det_i)
				if (key_press[1] == 'g'):
					det_i -= 1
					det_i = np.clip(det_i, 0, len(object_detections)-1)
					update_view = True
					# print('change det_i', det_i)
				if (key_press[1] == 'k'):
					exit()
					print('exited')

			if (click_press[0]):
				click_press[0] = False
				dists = np.sqrt((np.array(pgts)-click_press[2])**2 + (np.array(sigmas)-click_press[1])**2)
				det_i = np.argmin(dists)
				det_i = np.clip(det_i, 0, len(object_detections)-1)
				update_view = True

			if (update_view):
				# print('current det_i', det_i)
				draw_things(det_i)
				update_view = False
			if (update_object):
				update_object = False
				# print('current object_key', object_key)
				break

		fig.canvas.mpl_disconnect(f1)
		fig.canvas.mpl_disconnect(f2)











def eval():
	# load dataset
	dataset = SceneNetDataset('/media/feyre/DATA1/Datasets/SceneNetRGBD/pySceneNetRGBD/data/val')

	# load / create detections
	# model = load_model()
	model = EnsembleDetector.load_ensemble('/home/feyre/git_ws/object_detectors/pytorch-yolov3-eriklin/checkpoints/yolov3_nyu13_ensemble')
	# model = load_model()

	# define merging methods
	methods = MergingMethods()
	# dataset_instances = 0
	dataset_correct_pm = None
	VCMMS = []

	results = []
	cumulative_detections = []
	# dataset_detections = Detections()

	st = timelib.time()
	print('start')
	dataset_detections = pickle.load(open('dataset_detections_ensemble.pickle', 'rb'))
	print('end: {}'.format(timelib.time()-st))


	# loop over videos
	for video_i, video in enumerate(dataset):

		# if not (video_i == 8):
		# 	continue

		video_detections = dataset_detections.at_video(video_i)


		# box_errors = []
		# box_sigmas = []

		# # get detections for each image
		# video_detections = Detections()
		# for image_i, (image, targets) in enumerate(video):
		#
		# 	# print('image_keys', targets[:,0])
		# 	# if (image_i > 50):
		# 	# 	break
		#
		# 	# targets: [instance, label, box]
		# 	sys.stdout.write('[image] {} / {} | video {} / {}          \r'.format(image_i+1, len(video), video_i, len(dataset)))
		# 	sys.stdout.flush()
		#
		# 	# load / get image detections
		# 	detections = model.forward(image, scores=True)#, single=True)
		#
		# 	# detections = sample_pass(model, image, 20, scores=True)
		# 	# show_samples(image, detections)
		#
		# 	# associate detections -> instances
		# 	detections.associate(targets, iou_thres=0.2)
		#
		# 	# save video/image indicies
		# 	for detection in detections.list_detections:
		# 		detection.video_i = video_i
		# 		detection.image_i = image_i
		#
		# 	# draw_everything(image, targets, detections)
		#
		# 	# append to detections
		# 	# print('detections with length {}'.format(len(detections.list_detections)))
		# 	video_detections.add(detections)
		# 	# print('video detections with length {}'.format(len(video_detections.list_detections)))


		# 	# bbox data
		# 	detections.remove_unassociated()
		# 	for detection in detections.list_detections:
		# 		sigmas = detection.boxes.numpy().var(0)
		# 		instance_key = detection.instance_key
		# 		target = targets[targets[:,0].int()==instance_key,:][0]
		# 		diff = (detection.mean_box() - target[2:6]).numpy()
		# 		box_errors += [abs(e) for e in diff]
		# 		box_sigmas += [v for v in sigmas]
		#
		# print(max(box_errors), max(box_sigmas))
		# plt.scatter(box_sigmas, box_errors, marker='x')
		# plt.title('line error vs measured variance')
		# plt.xlabel('variance')
		# plt.ylabel('error')
		# plt.show()

		video_detections.remove_unassociated()
		if (len(video_detections) == 0):
			continue

		# n_obj = len(list(video_detections.per_instance().items()))
		# n_bad = video_detections.remove_bad_objects(0.9, video.instance_to_nyu)
		# n_obj2 = len(list(video_detections.per_instance().items()))
		# print('removed {}/{} bad objects, {} remaining'.format(n_bad,n_obj, n_obj2))

		# dataset_detections.add(video_detections)
		# continue

		# pickle.dump(dataset_detections, open('dataset_detections_ensemble.pickle', 'wb'))
		# if False:

		# inspect_data(video_detections, video.instance_to_nyu)
		# inspect_uncertainty(video_detections, video.instance_to_nyu, dataset)



		for instance_id, object_detections in video_detections.per_instance().items():
			nyu_label = video.instance_to_nyu[instance_id]['nyu']
			for od in object_detections:
				distribution = torch.nn.functional.softmax(od.distributions, dim=1).mean(0).numpy()
				detection = {
								'instance_id':instance_id,
								'confidence_gt':(distribution[nyu_label]),
								'correct':(distribution.argmax()==nyu_label),
							}
				cumulative_detections.append(detection)
				# print((od.max_class()==nyu_label).item())






		# loop over each instance
		for instance_id, object_detections in video_detections.per_instance().items():
			n_samples = len(object_detections)
			nyu_label = video.instance_to_nyu[instance_id]['nyu']
			name = video.instance_to_nyu[instance_id]['name']

			# calculate object percentage correct
			correct = np.array([True if (o.max_class() == nyu_label) else False for o in object_detections])
			percentage_correct = correct.sum() / len(correct)

			scores = torch.stack([ detection.distributions.mean(0) 	for detection in object_detections]).numpy()
			probabilities = torch.stack([ torch.nn.functional.softmax(detection.distributions, dim=1).mean(0) 	for detection in object_detections]).numpy()
			PGT_avg = probabilities[:,nyu_label].mean()

			MMGT_avg = (1-(probabilities.max(1) - probabilities[:,nyu_label])).mean()


			object_conditions = PGT_avg

			# if (n_samples < 50):
			# 	continue




			# implement merging strategies
			method_names, method_values = methods.merge(object_detections)
			estimated_labels = method_values.argmax(dim=1)
			# method_values = (Mx13)

			# 1.0 correctness
			m1 = (estimated_labels == nyu_label).tolist() # (M)

			# 2.0 probability of GT
			m2 = method_values[:, nyu_label].tolist()

			# 3.0 MSDE
			delta = torch.zeros(method_values.shape)
			delta[:, nyu_label] = 1
			m3 = (-(delta-method_values)**2).mean(1).tolist()

			# 4.0 MGR
			m4 = (-method_values.max(1)[0] / method_values[:, nyu_label].clamp(0.01,1.0)).tolist()

			for ii, mv in enumerate(method_values):
				if (~torch.isclose(mv.sum(), torch.Tensor([1.0]))):
					print('{} | not SUM to 1'.format(method_names[ii]))
				if ~((mv.max() < 1.0) & (mv.min() > 0.0)):
					if (mv.max() > 1.0):
						if ~(torch.isclose(mv.max(), torch.Tensor([1.0]))):
							print('{} | not 1-0'.format(method_names[ii]))
					if (mv.min() < 0.0):
						if ~(torch.isclose(mv.min(), torch.Tensor([0.0]))):
							print('{} | not 1-0'.format(method_names[ii]))

			metric_names = ['correctness', 'PGT', 'MSDE', 'MGR']
			metrics = [m1,m2,m3,m4]
			# print(np.array(metrics))

			# each metric is M long vector
			# for i, method_name in enumerate(method_names):
			# 	point = {
			# 				'conditions':percentage_correct,
			# 				'method':method_name,
			# 				'samples':len(object_detections),
			# 			 }
			# 	for j, metric_name in enumerate(metric_names):
			# 		point[metric_name] = metrics[j][i]
			# 	results.append(point)

			for i, method_name in enumerate(method_names):
				for j, metric_name in enumerate(metric_names):
					object = {
								'conditions':object_conditions,
								'method':method_name,
								'metric':metric_name,
								'samples':len(object_detections),
								'samples_per_view':[len(o.boxes) for o in object_detections],
								'value':metrics[j][i],
							 }
					results.append(object)


		if (video_i != (len(dataset)-1)) and (video_i != 100):
			continue




		# # PLOT cumulative detections
		# detections_df = pd.DataFrame(cumulative_detections)
		# bins = pd.interval_range(start=0.0,end=1.0,periods=10)
		# detections_df['binned_confidence'] = pd.cut(detections_df['confidence_gt'], bins)
		#
		# xs = []
		# heights = []
		# for group in detections_df.groupby('binned_confidence'):
		# 	xs.append(group[0].mid)
		# 	corrects = group[1].correct.to_numpy()
		# 	if (len(corrects) > 0):
		# 		heights.append(corrects.mean())
		# 	else:
		# 		heights.append(0)
		#
		# plt.bar(xs,heights, width=0.1, edgecolor='black')
		# plt.show()










		results_df = pd.DataFrame(results)
		results_df['binned_conditions'] = pd.cut(results_df['conditions'], 10)



		# import code
		# code.interact(local=locals())
		print('\nn_objects: ',len(results_df[(results_df.method=='average_scores')&(results_df.metric=='correctness')]))

		for g in results_df.groupby(['binned_conditions']):
			df = g[1]
			df = df[(df.method=='average_scores')&(df.metric=='correctness')]
			print('Conditions: {}'.format(g[0]))
			print('    n_objects: {}'.format(len(df)))
			print('    n_views (avg): {}'.format(np.mean(df.samples.values)))
			print('    n_samples (avg): {}'.format(np.mean( [np.mean(x) for x in df.samples_per_view.values.tolist()] )))
			# print('samples_per_view_per_object:')
			# print(df.samples_per_view.values.tolist())
			# print('')


		results_grouped = results_df.groupby(['method', 'metric'])



		print('[eval] video {} / {}'.format(video_i, len(dataset)))

		# # LINE PLOT
		for i, metric_name in enumerate(metric_names):
			plt.subplot(2,2,i+1)
			for j, method_name in enumerate(method_names):
				conditions = []
				values = []
				for g in results_grouped.get_group((method_name, metric_name)).groupby('binned_conditions'):
					scores = g[1].value.to_numpy()
					if (len(scores) >= 1):
						conditions.append(g[0].mid)
						values.append(g[1].value.to_numpy().mean())

				plt.plot(conditions, values, label=method_name)
			plt.ylabel(metric_name)
			plt.xlim(1.0,0.0)
		plt.legend()
		plt.show()
		print('done')


		# # PLOT SCATTER
		# for j, method_name in enumerate(method_names):
		# 	fig = plt.figure(j)
		# 	fig.suptitle(method_name)
		# 	for i, metric_name in enumerate(metric_names):
		# 		plt.subplot(2,2,i+1)
		#
		# 		g = results_grouped.get_group((method_name, metric_name))
		# 		v = g.value.to_numpy()
		# 		c = g.conditions.to_numpy()
		# 		plt.scatter(c,v, marker='x')
		# 		plt.xlim(1.0,0.0)
		# plt.show()


		# # BOX PLOT
		# for j, method_name in enumerate(method_names):
		# 	fig = plt.figure(j)
		# 	fig.suptitle(method_name)
		# 	for i, metric_name in enumerate(metric_names):
		# 		plt.subplot(2,2,i+1)
		#
		# 		conditions = []
		# 		values = []
		# 		for g in results_grouped.get_group((method_name, metric_name)).groupby('binned_conditions'):
		# 			scores = g[1].value.to_numpy()
		# 			if (len(scores) >= 1):
		# 				conditions.append(np.round(g[0].mid,2))
		# 				values.append(g[1].value.to_numpy())
		#
		# 		plt.boxplot(values, positions=conditions, widths=0.08)
		# 		plt.xlim(1.0,0.0)
		#
		# plt.show()









		#
		# 	for t in VCMMS:
		# 		print(t.shape)
		# 	_CMMS = np.stack(VCMMS).mean(0)
		#
		#
		# 	for i in range(_CMMS.shape[1]):
		# 		plt.subplot(2,2,i+1)
		# 		for j in range(_CMMS.shape[2]):
		# 			plt.plot(difficulties, _CMMS[:,i,j], label=method_names[j])
		# 		plt.ylabel(metric_names[i])
		# 		plt.xlim(1.0,0.0)
		# 	plt.legend()
		# 	plt.show()
		#
		# 	print('[eval] ')





		# cluster by metrics
		# clustered_results = defaultdict(list)
		# for point in results:
		# 	key = '{}:{}:{}'.format(point['conditions'], point['metric'], point['method'])
		# 	clustered_results[key].append(point['value'])



		# video_detections.eval_uncertainty(video.instance_to_nyu)


		# loop over each instance in n_views order, plotting detections
		# instance_detections = sorted(video_detections.per_instance().items(), key = lambda item : len(item[1]))
		# for instance_id, object_detections in instance_detections[::-1]:
		# 	nyu_label = video.instance_to_nyu[instance_id]['nyu']
		# 	print('n_samples is: {}'.format(len(object_detections)))
		# 	print('correct index is: {}\n'.format(nyu_label))
		#
		# 	# investigate distribution of detections
		# 	samples = torch.stack([ torch.FloatTensor([detection.distributions.shape[0]]*detection.distributions.shape[1]) for detection in object_detections]).numpy()
		# 	scores = torch.stack([ detection.distributions.mean(0) 												for detection in object_detections]).numpy()
		# 	sigmoids = torch.stack([ torch.sigmoid(detection.distributions).mean(0) 							for detection in object_detections]).numpy()
		# 	probabilities = torch.stack([ torch.nn.functional.softmax(detection.distributions, dim=1).mean(0) 	for detection in object_detections]).numpy()
		# 	k = np.repeat(np.arange(scores.shape[1])[:,np.newaxis], scores.shape[0], 1).T
		#
		# 	# plot dist
		# 	import matplotlib.pyplot as plt
		#
		# 	fig = plt.figure()
		# 	ax = fig.gca()
		# 	ax.scatter(k, scores, marker='x', c=k, cmap=plt.get_cmap('rainbow'))
		# 	fig2 = plt.figure()
		# 	ax2 = fig2.gca()
		# 	ax2.scatter(k, sigmoids, marker='x', c=k, cmap=plt.get_cmap('rainbow'))
		# 	fig3 = plt.figure()
		# 	ax3 = fig3.gca()
		# 	ax3.scatter(k, probabilities, marker='x', c=k, cmap=plt.get_cmap('rainbow'))
		# 	plt.show()
		#
		# 	# fig, ax = plt.subplots(1,13)
		# 	# for i, col in enumerate(ax):
		# 	# 	col.hist(scores[:,i], orientation='horizontal', bins=80)
		# 	# plt.show()




		# results = torch.FloatTensor(results).permute(1,0) # (method x instance)
		# dataset_instances += video_correctness.shape[1]
		# if (dataset_correct_pm is None):
		# 	dataset_correct_pm = video_correctness.sum(1)
		# else:
		# 	dataset_correct_pm += video_correctness.sum(1)


		# print video evaluation
		# for method_i, method_name in enumerate(method_names):
		# 	print('    [eval] {} | Correctness {:.3f}'.format(method_name, video_correctness[method_i].mean()))

		# print('[eval] videos with {} instances'.format(dataset_instances))
		# for method_i, method_name in enumerate(method_names):
		# 	print('    [eval] {name: <{fill}} | Correctness {score:.3f}'.format(name=method_name, score=dataset_correct_pm[method_i]/float(dataset_instances), fill='28'))




def show_samples(image, observations):
	import matplotlib.pyplot as plt
	import matplotlib.patches as patches
	from scenenetvideo import NYU_13_CLASSES
	cvimage = image.permute(1,2,0).numpy()

	for detections in observations:
		fig, ax = plt.subplots(1)
		ax.clear()
		ax.imshow(cvimage)
		for detection_i, detection in enumerate(detections):
			c = 'b' if (detection_i==0) else 'r'
			box = detection[0:4]
			rect = patches.Rectangle((box[0], box[1]), box[2]-box[0], box[3]-box[1], edgecolor=c, facecolor='none')
			ax.add_patch(rect)
		plt.show()


def show_outputs(image, outputs):
	import matplotlib.pyplot as plt
	import matplotlib.patches as patches
	cvimage = image.permute(1,2,0).numpy()


	for output in outputs:
		class_score, class_index = output[5:].max(0, keepdim=True)
		print('[det] class_name ({}) | obj ({:.3f}) | class_score ({:.3f})'.format(NYU_13_CLASSES[class_index], output[4], class_score.item()))
		print('[det] box {}'.format(output[0:4].int()))

		fig, ax = plt.subplots(1)
		ax.clear()
		ax.imshow(cvimage)
		box = output[0:4]
		rect = patches.Rectangle((box[0], box[1]), box[2]-box[0], box[3]-box[1], edgecolor='r', facecolor='none')
		ax.add_patch(rect)
		plt.show()

def show_boxes(image, boxes):
	import matplotlib.pyplot as plt
	import matplotlib.patches as patches
	from scenenetvideo import NYU_13_CLASSES
	cvimage = image.permute(1,2,0).numpy()


	fig, ax = plt.subplots(1)
	ax.clear()
	ax.imshow(cvimage)
	for box in boxes:
		box = box[0:4]
		rect = patches.Rectangle((box[0], box[1]), box[2]-box[0], box[3]-box[1], edgecolor='r', facecolor='none')
		ax.add_patch(rect)
	plt.show()


if __name__ == '__main__':
	start_t = timelib.time()
	eval()
	end_t = timelib.time()

	print('Time Elapsed: {}s'.format(end_t-start_t))
	print('Time Elapsed: {}m'.format((end_t-start_t)/60.))
	print('Time Elapsed: {}h'.format((end_t-start_t)/60.**2))
