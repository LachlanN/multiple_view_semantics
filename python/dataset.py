# Copyright (c) 2018, Lachlan Nicholson, ARC Centre of Excellence for Robotic Vision, Queensland University of Technology (QUT)
# All rights reserved.
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Created by Lachlan on 12/12/18.
#

# import standard libraries
from __future__ import print_function
import numpy as np
import sys, os
from PIL import Image
from scipy import misc
from skimage.transform import resize
import random
import importlib
import time as timelib
import json
import errno

from PIL import ImageDraw
import torch
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms as transforms
import torch.nn.functional as F

import re
def tryint(s):
	try:
		return int(s)
	except ValueError:
		return s
def alphanum_key(s):
	""" Turn a string into a list of string and number chunks.
		"z23a" -> ["z", 23, "a"]
	"""
	return [ tryint(c) for c in re.split('([0-9]+)', s) ]
def sort_nicely(l):
	""" Sort the given list in the way that humans expect.
	"""
	# l.sort(key=alphanum_key)
	return sorted(l, key=alphanum_key)

def horisontal_flip(images, targets):
	images = torch.flip(images, [-1])
	if (not targets is None):
		targets[:, 2] = 1 - targets[:, 2]
	return images, targets




def pad_to_square(img, pad_value):
	c, h, w = img.shape
	dim_diff = np.abs(h - w)
	# (upper / left) padding and (lower / right) padding
	pad1, pad2 = dim_diff // 2, dim_diff - dim_diff // 2
	# Determine padding
	pad = (0, 0, pad1, pad2) if h <= w else (pad1, pad2, 0, 0)
	# Add padding
	img = F.pad(img, pad, "constant", value=pad_value)

	return img, pad


def resize(image, size):
	image = F.interpolate(image.unsqueeze(0), size=size, mode="nearest").squeeze(0)
	return image


def random_resize(images, min_size=288, max_size=448):
	new_size = random.sample(list(range(min_size, max_size + 1, 32)), 1)[0]
	images = F.interpolate(images, size=new_size, mode="nearest")
	return images



NYU_13_CLASSES = ['Bed',
				  'Books',
				  'Ceiling',
				  'Chair',
				  'Floor',
				  'Furniture',
				  'Objects',
				  'Picture',
				  'Sofa',
				  'Table',
				  'TV',
				  'Wall',
				  'Window']

NYU_WNID_TO_CLASS = {
	'04593077':4, '03262932':4, '02933112':6, '03207941':7, '03063968':10, '04398044':7, '04515003':7,
	'00017222':7, '02964075':10, '03246933':10, '03904060':10, '03018349':6, '03786621':4, '04225987':7,
	'04284002':7, '03211117':11, '02920259':1, '03782190':11, '03761084':7, '03710193':7, '03367059':7,
	'02747177':7, '03063599':7, '04599124':7, '20000036':10, '03085219':7, '04255586':7, '03165096':1,
	'03938244':1, '14845743':7, '03609235':7, '03238586':10, '03797390':7, '04152829':11, '04553920':7,
	'04608329':10, '20000016':4, '02883344':7, '04590933':4, '04466871':7, '03168217':4, '03490884':7,
	'04569063':7, '03071021':7, '03221720':12, '03309808':7, '04380533':7, '02839910':7, '03179701':10,
	'02823510':7, '03376595':4, '03891251':4, '03438257':7, '02686379':7, '03488438':7, '04118021':5,
	'03513137':7, '04315948':7, '03092883':10, '15101854':6, '03982430':10, '02920083':1, '02990373':3,
	'03346455':12, '03452594':7, '03612814':7, '06415419':7, '03025755':7, '02777927':12, '04546855':12,
	'20000040':10, '20000041':10, '04533802':7, '04459362':7, '04177755':9, '03206908':7, '20000021':4,
	'03624134':7, '04186051':7, '04152593':11, '03643737':7, '02676566':7, '02789487':6, '03237340':6,
	'04502670':7, '04208936':7, '20000024':4, '04401088':7, '04372370':12, '20000025':4, '03956922':7,
	'04379243':10, '04447028':7, '03147509':7, '03640988':7, '03916031':7, '03906997':7, '04190052':6,
	'02828884':4, '03962852':1, '03665366':7, '02881193':7, '03920867':4, '03773035':12, '03046257':12,
	'04516116':7, '00266645':7, '03665924':7, '03261776':7, '03991062':7, '03908831':7, '03759954':7,
	'04164868':7, '04004475':7, '03642806':7, '04589593':13, '04522168':7, '04446276':7, '08647616':4,
	'02808440':7, '08266235':10, '03467517':7, '04256520':9, '04337974':7, '03990474':7, '03116530':6,
	'03649674':4, '04349401':7, '01091234':7, '15075141':7, '20000028':9, '02960903':7, '04254009':7,
	'20000018':4, '20000020':4, '03676759':11, '20000022':4, '20000023':4, '02946921':7, '03957315':7,
	'20000026':4, '20000027':4, '04381587':10, '04101232':7, '03691459':7, '03273913':7, '02843684':7,
	'04183516':7, '04587648':13, '02815950':3, '03653583':6, '03525454':7, '03405725':6, '03636248':7,
	'03211616':11, '04177820':4, '04099969':4, '03928116':7, '04586225':7, '02738535':4, '20000039':10,
	'20000038':10, '04476259':7, '04009801':11, '03909406':12, '03002711':7, '03085602':11, '03233905':6,
	'20000037':10, '02801938':7, '03899768':7, '04343346':7, '03603722':7, '03593526':7, '02954340':7,
	'02694662':7, '04209613':7, '02951358':7, '03115762':9, '04038727':6, '03005285':7, '04559451':7,
	'03775636':7, '03620967':10, '02773838':7, '20000008':6, '04526964':7, '06508816':7, '20000009':6,
	'03379051':7, '04062428':7, '04074963':7, '04047401':7, '03881893':13, '03959485':7, '03391301':7,
	'03151077':12, '04590263':13, '20000006':1, '03148324':6, '20000004':1, '04453156':7, '02840245':2,
	'04591713':7, '03050864':7, '03727837':5, '06277280':11, '03365592':5, '03876519':8, '03179910':7,
	'06709442':7, '03482252':7, '04223580':7, '02880940':7, '04554684':7, '20000030':9, '03085013':7,
	'03169390':7, '04192858':7, '20000029':9, '04331277':4, '03452741':7, '03485997':7, '20000007':1,
	'02942699':7, '03231368':10, '03337140':7, '03001627':4, '20000011':6, '20000010':6, '20000013':6,
	'04603729':10, '20000015':4, '04548280':12, '06410904':2, '04398951':10, '03693474':9, '04330267':7,
	'03015149':9, '04460038':7, '03128519':7, '04306847':7, '03677231':7, '02871439':6, '04550184':6,
	'14974264':7, '04344873':9, '03636649':7, '20000012':6, '02876657':7, '03325088':7, '04253437':7,
	'02992529':7, '03222722':12, '04373704':4, '02851099':13, '04061681':10, '04529681':7,}


class SceneNetImages(torch.utils.data.Dataset):
	'''
	Loads SceneNet from folders,
	Labels in WNID or NYU_13 format.
	'''
	def __init__(self, root_dir, image_size=416, max_images=None, augment=True, multiscale=True, verbose=False):
		'''
		Loads a list of [rgb_images, instance_images, instance_to_class]
		For effeciency, only loads protobuf for each primary once. (hence, render order looping)
		'''

		protobuf_def_path = '/media/feyre/DATA1/Datasets/SceneNetRGBD/pySceneNetRGBD/scenenet_pb2.py'
		self.verbose = verbose
		self.image_size = image_size
		self.max_objects = 100
		self.augment = augment
		self.multiscale = multiscale
		self.min_size = self.image_size - 3 * 32
		self.max_size = self.image_size + 3 * 32
		self.batch_count = 0
		self.normalized_labels = True


		# extract folder / file information
		protobuf_def_folder = protobuf_def_path.replace(protobuf_def_path.split('/')[-1], '')
		protobuf_def_file = protobuf_def_path.replace(protobuf_def_folder, '')

		# load protobuf definition
		sys.dont_write_bytecode = True
		sys.path.append(os.path.realpath(protobuf_def_folder))
		sn = importlib.import_module(protobuf_def_file.replace('.py',''))

		# get protobuf folder from dataset name
		dataset_name = root_dir.split('/')[-1] if (not root_dir.endswith('/')) else root_dir.split('/')[-2]
		protobuf_folder = os.path.join(os.path.realpath(os.path.join(root_dir, '../')), '{}_protobufs'.format(dataset_name))



		# collect images
		print('Loading image filepaths:')
		start_t = timelib.time()

		primary_folders = [fn for fn in os.listdir(root_dir)
								if os.path.isdir(os.path.join(root_dir, fn))]
		pf_t = timelib.time()
		print('Primary Folders: {:.3f}s'.format(pf_t-start_t))

		secondary_folders = [[fn for fn in os.listdir(os.path.join(root_dir, pf))
								if (not '.' in fn)]
									for pf in primary_folders]
		sf_t = timelib.time()
		print('Secondary Folders: {:.3f}s'.format(sf_t-pf_t))

		# limit dataset size via:
		# - sorted (n_images)
		# - random (n_images)
		# - equal images per video (n_imgs_per_video)
		# - equal images per class (n_imgs_per_class)

		# image_paths = [os.path.join(root_dir, pf, sf, 'photo', fn)
		# 						for pf, sfs in zip(primary_folders, secondary_folders)
		# 							for sf in sfs
		# 								for fn in os.listdir(os.path.join(root_dir, pf, sf, 'photo'))
		# 									if fn.endswith('.jpg')]

		image_paths = [os.path.join(root_dir, pf, sf, 'photo', '{}.jpg'.format(n))
								for pf, sfs in zip(primary_folders, secondary_folders)
									for sf in sfs
										for n in range(0,7476,25)]

		image_paths = np.array((image_paths))

		if (not max_images is None):
			np.random.seed(23)
			image_paths = image_paths[(np.random.choice(len(image_paths), max_images, replace=False))]

		# image_paths = []
		# for pf, sfs in zip(primary_folders, secondary_folders):
		# 	for sf in sfs:
		# 		for fn in os.listdir(os.path.join(root_dir, pf, sf, 'photo')):
		# 			if fn.endswith('.jpg'):
		# 				image_paths.append(os.path.join(root_dir, pf, sf, 'photo', fn))
		# 				if ((not max_images is None) and (len(image_paths) >= max_images)):
		# 					break
		# 		if ((not max_images is None) and (len(image_paths) >= max_images)):
		# 			break
		# 	if ((not max_images is None) and (len(image_paths) >= max_images)):
		# 		break

		f_t = timelib.time()
		print('Files: {:.3f}s'.format(f_t-sf_t))

		instance_paths = [ip.replace('photo','instance').replace('.jpg','.png') for ip in image_paths]
		boxes_paths = [ip.replace('photo','boxes').replace('.jpg','.json') for ip in image_paths]
		i_t = timelib.time()
		print('Instances: {:.3f}s'.format(i_t-f_t))



		render_paths = [ip.split('/photo')[0].split('{}/'.format(dataset_name))[-1] for ip in image_paths]
		rp_t = timelib.time()
		print('Render Paths: {:.3f}s'.format(rp_t-i_t))

		# recalculate primary/secondary from render_paths (if some videos not needed)
		# if (not max_images is None):
		# 	import itertools
		# 	primary_folders = np.unique([rp.split('/')[0] for rp in render_paths])
		# 	secondary_folders = [[s.split('/')[-1] for s in list(v)] for _,v in itertools.groupby(np.unique(render_paths), key=lambda x:x.split('/')[0])]

		# collect protobuf names
		protobuf_names = ['scenenet_rgbd_{}_{}.pb'.format(dataset_name, primary_folder) for primary_folder in primary_folders]

		# load protobufs
		render_to_info = {}
		for primary_folder, _secondary_folders, protobuf_name in zip(primary_folders, secondary_folders, protobuf_names):
			trajectories = sn.Trajectories()
			with open(os.path.join(protobuf_folder, protobuf_name),'rb') as f:
				trajectories.ParseFromString(f.read())

			for secondary_folder in _secondary_folders:
				render_path = '{}/{}'.format(primary_folder, secondary_folder)
				instance_info = self.get_instance_info(trajectories, render_path)
				render_to_info[render_path] = instance_info
		proto_t = timelib.time()
		print('Protobufs: {:.3f}s'.format(proto_t-rp_t))

		print('Total: {:.3f}s\n'.format(proto_t - start_t))
		print('With: ({}) images, ({}) instances, ({}) paths, ({}) videos'.format(len(image_paths), len(instance_paths), len(render_paths), len(render_to_info)))

		# stored in memory
		self.image_paths = image_paths
		self.instance_paths = instance_paths
		self.boxes_paths = boxes_paths
		self.render_paths = render_paths
		self.render_to_info = render_to_info


	def get_instance_info(self, trajectories, render_name):
		instance_info = {}

		# get correct trajectory
		trajectory = [t for t in trajectories.trajectories if (str(t.render_path)==render_name)]
		assert (len(trajectory) == 1)
		trajectory = trajectory[0]

		# get instance info (wnid, name, nyu) from
		for instance in trajectory.instances:
			instance_info[int(instance.instance_id)] = {'wnid': str(instance.semantic_wordnet_id),
														'name': str(instance.semantic_english)}
														# 'nyu': (NYU_WNID_TO_CLASS[str(instance.semantic_wordnet_id) - 1)]}

		return instance_info

	def get_labels(self, instance_image, instance_info):
		'''
		Converts an instance image into suitable class boxes
		Returns linear structure [instance_id, %cx,%cy,%w,%h] (n_instances, 5):
			- ignore instance 0 and instances without wordnet ids
			- ignore small instances
			- handle occlusions by splitting instances
		'''
		h, w = instance_image.shape
		instance_ids = np.unique(instance_image)

		boxes = []
		for i, instance_id in enumerate(instance_ids):

			# get masks
			instance_mask = (instance_image == instance_id)
			horizontal_mask = np.any(instance_mask, axis=0)
			vertical_mask = np.any(instance_mask, axis=1)

			# calculate bounds
			xmin, xmax = np.where(horizontal_mask)[0][[0,-1]]
			ymin, ymax = np.where(vertical_mask)[0][[0,-1]]

			# normalize box coordinates
			box = np.array([xmin, ymin, xmax, ymax])
			# nbox = self.normalize_box(box, w, h)

			# check if sn trajectory contains instance id
			if (not (int(instance_id) in instance_info)):
				print('[warn] instance {} does not exist'.format(instance_id))
				continue

			# convert instance ID to information
			wordnet_id = instance_info[int(instance_id)]['wnid']
			name = instance_info[int(instance_id)]['name']


			# convert wordnet to NYU
			if (not wordnet_id == ''):
				nyu13_id = NYU_WNID_TO_CLASS[wordnet_id]-1
			else:
				nyu13_id = ''

			boxes.append({'instance_id': int(instance_id),
						  'wordnet_id': wordnet_id,
						  'nyu13_id': nyu13_id,
						  'name': name,
						  'box': box.tolist()})

		return boxes

	def normalize_box(self, box, w, h):
		'''
		Converts [xmin,ymin,xmax,ymax] -> [%cx,%cy,%w,%h]
		'''
		nbox = [float(box[0]+(box[2]-box[0])/2.0) / w,
				float(box[1]+(box[3]-box[1])/2.0) / h,
				float(box[2]-box[0]) / w,
				float(box[3]-box[1]) / h]
		return nbox
	def denormalize_box(self, nbox, w, h):
		'''
		Converts [%cx,%cy,%w,%h] -> [xmin, ymin, xmax, ymax]
		'''
		box = [int(round((nbox[0]-(nbox[2])/2.0) * w)),
			   int(round((nbox[1]-(nbox[3])/2.0) * h)),
			   int(round((nbox[0]+(nbox[2])/2.0) * w)),
			   int(round((nbox[1]+(nbox[3])/2.0) * h))]
		return box

	def __getitem__(self, index):
		'''
		1. Load rgb/instance image
		2. Calculate boxes
		3. Pad rgb image
		4. Pad boxes
		return [rgb, instance, boxes]
		'''

		time_start = timelib.time()

		# load images
		image_path = self.image_paths[index % len(self.image_paths)]
		image = transforms.ToTensor()(Image.open(image_path).convert('RGB'))

		# pad image
		_, h, w = image.shape
		h_factor, w_factor = (h, w) if self.normalized_labels else (1, 1)

		# Pad to square resolution
		input_img, pad = pad_to_square(image, 0)
		_, padded_h, padded_w = input_img.shape


		# check for old jsons
		mistake_path = self.instance_paths[index % len(self.image_paths)].replace('.png', '.json')
		if (os.path.isfile(mistake_path)):
			os.remove(mistake_path)

		# LOAD DETECTIONS
		loading_failed = False
		boxes_path = self.boxes_paths[index % len(self.image_paths)]
		if (os.path.exists(boxes_path)):
			try:
				boxes = json.load(open(boxes_path))
			except:
				loading_failed = True

		# CALCULATE DETECTIONS
		if (not os.path.exists(boxes_path)) or (loading_failed):
			# load instance image
			instance_path = self.instance_paths[index % len(self.image_paths)]
			instance_image = np.array(Image.open(instance_path))

			# convert instance_ids to nyu
			render_path = self.render_paths[index % len(self.render_paths)]
			instance_info = self.render_to_info[render_path]

			# get boxes from instance image
			boxes = self.get_labels(instance_image, instance_info)

			# # WARNING: not thread safe
			# if (not os.path.exists(os.path.dirname(boxes_path))):
			try:
				os.mkdir(os.path.dirname(boxes_path))
			except OSError as e:
				if e.errno != errno.EEXIST:
					raise

			# save for future
			with open(boxes_path, 'w') as f:
				json.dump(boxes, f)

		# fill targets [batch, nyu13, x1,y1,x2,y2]
		width_thres = 5
		height_thres = 5

		# flatten boxes, ignore small objects
		flat_boxes = []
		for box in boxes:
			# ignore bad boxes
			if (box['wordnet_id'] == ''):
				continue
			if (box['instance_id'] == 0):
				continue
			width = box['box'][2] - box['box'][0]
			height = box['box'][3] - box['box'][1]
			if ((width < width_thres) or (height < height_thres)):
				continue

			flat_boxes.append([float(box['nyu13_id'])]+box['box'])
		flat_boxes = torch.FloatTensor(flat_boxes)

		# flat boxes = (n, 5) [nyu_id, xmin, ymin, xmax, ymax]
		# targets = (n, 6) [0, nyu_id, %cx, %cy, %w, %h]

		targets = None
		if (len(flat_boxes) > 0):
			targets = torch.zeros((len(flat_boxes), 6))

			# Adjust for added padding
			flat_boxes[:, 1] += pad[0]
			flat_boxes[:, 2] += pad[2]
			flat_boxes[:, 3] += pad[1]
			flat_boxes[:, 4] += pad[3]

			# Returns (x, y, w, h)
			targets[:, 2] = ((flat_boxes[:, 1] + flat_boxes[:, 3]) / 2) / padded_w
			targets[:, 3] = ((flat_boxes[:, 2] + flat_boxes[:, 4]) / 2) / padded_h
			targets[:, 4] = (flat_boxes[:, 3] - flat_boxes[:, 1]) / padded_w
			targets[:, 5] = (flat_boxes[:, 4] - flat_boxes[:, 2]) / padded_h
			targets[:, 1] = flat_boxes[:,0]


		# if (len(boxes) < 1):
		# 	print('[dataset] index ({}) failed to get any GT with path\n {}'.format(index, image_path))
		# 	self.image_paths.pop(index%len(self.image_paths))
		# 	self.instance_paths.pop(index%len(self.instance_paths))
		# 	self.render_paths.pop(index%len(self.render_paths))
		#
		# 	# WARNING: can be recursively infinite if too many images with no data
		# 	return self.__getitem__(index)



		## inspect
		# import matplotlib.pyplot as plt
		# tensor_image = (input_img.numpy()*255.0).astype('uint8')
		# tensor_image = tensor_image.transpose((1,2,0))
		#
		# for target in targets:
		# 	pil_image = Image.fromarray(tensor_image, 'RGB')
		# 	# pil_image = Image.open(image_path).convert('RGB')
		#
		# 	nbox = target[2:].numpy()
		# 	box = self.denormalize_box(nbox, padded_w, padded_h)
		# 	# box = self.denormalize_box(nbox, w, h)
		# 	print(NYU_13_CLASSES[int(target[1])], box)
		#
		# 	draw = ImageDraw.Draw(pil_image)
		# 	draw.rectangle(box, outline=(255,0,0))
		# 	del draw
		# 	plt.imshow(pil_image)
		# 	plt.show()


		# Apply augmentations
		if self.augment:
			if np.random.random() < 0.5:
				input_img, targets = horisontal_flip(input_img, targets)

		time_end = timelib.time()
		# print('[dataset] loaded in {:.5f} seconds'.format(time_end-time_start))
		return image_path, input_img, targets








	def collate_fn(self, batch):
		paths, imgs, targets = list(zip(*batch))
		# Remove empty placeholder targets
		targets = [boxes for boxes in targets if boxes is not None]
		# Add sample index to targets
		for i, boxes in enumerate(targets):
			boxes[:, 0] = i
		targets = torch.cat(targets, 0)
		# Selects new image size every tenth batch
		# print('ms', self.multiscale)
		# print('f1', (self.batch_count % 10 == 0))
		# print('flag', ((self.multiscale) and (self.batch_count % 10 == 0)))
		# if ((self.multiscale) and (self.batch_count % 10 == 0)):
		# 	self.image_size = random.choice(range(self.min_size, self.max_size + 1, 32))
		# Resize images to input shape
		imgs = torch.stack([resize(img, self.image_size) for img in imgs])
		self.batch_count += 1
		return paths, imgs, targets

	def __len__(self):
		return len(self.image_paths)




if __name__ == '__main__':
	train_path = '/media/feyre/DATA1/Datasets/SceneNetRGBD/pySceneNetRGBD/data/train/'
	valid_path = '/media/feyre/DATA1/Datasets/SceneNetRGBD/pySceneNetRGBD/data/val/'

	dataset = SceneNetImages(train_path, augment=True, multiscale=True, verbose=True)

	# torch.manual_seed(3)
	# major_dataloader = torch.utils.data.DataLoader(
	# 	dataset,
	# 	batch_size=50000,
	# 	shuffle=True,
	# 	num_workers=0,
	# 	pin_memory=True,
	# 	collate_fn=dataset.collate_fn,
	# )
	#
	# for epoch in range(10):
	# 	for major_batch_i, major_batch in enumerate(major_dataloader):
	#
	# 			minor_dataloader = torch.utils.data.DataLoader(
	# 				major_dataloader,
	# 				batch_size=8,
	# 				shuffle=True,
	# 				num_workers=0,
	# 			)
	#
	# 			for minor_batch_i, minor_batch in enumerate(minor_dataloader)





# eof
