# Copyright (c) 2018, Lachlan Nicholson, ARC Centre of Excellence for Robotic Vision, Queensland University of Technology (QUT)
# All rights reserved.
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Created by Lachlan on 12/12/18.
#

# import standard libraries
from __future__ import print_function
import numpy as np
import json, sys, os, argparse
from PIL import Image
from scipy import misc
from skimage.transform import resize
from copy import deepcopy
import importlib
import torch

# import shared libraries
sys.dont_write_bytecode = True
from dataset import SceneNetImages

# setup dataset (SceneNet)



import re
def tryint(s):
	try:
		return int(s)
	except ValueError:
		return s
def alphanum_key(s):
	""" Turn a string into a list of string and number chunks.
		"z23a" -> ["z", 23, "a"]
	"""
	return [ tryint(c) for c in re.split('([0-9]+)', s) ]
def sort_nicely(l):
	""" Sort the given list in the way that humans expect.
	"""
	l.sort(key=alphanum_key)


def iou(boxA, boxB):
	# determine the (x, y)-coordinates of the intersection rectangle
	xA = max(boxA[0], boxB[0])
	yA = max(boxA[1], boxB[1])
	xB = min(boxA[2], boxB[2])
	yB = min(boxA[3], boxB[3])

	# compute the area of intersection rectangle
	interArea = max(0, max(0,(xB - xA + 1)) * max(0,(yB - yA + 1)))

	# compute the area of both the prediction and ground-truth boxes
	boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
	boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

	# compute the intersection over union by taking the intersection
	# area and dividing it by the sum of prediction + ground-truth
	# areas - the interesection area
	iou = interArea / float(boxAArea + boxBArea - interArea)

	if iou < 0.0:
		print('NEGATIVE IOU')
		exit()
	return iou

def cv2_draw_box(image, box, color=(0,0,255), thickness=2):
	cv2.rectangle(image, (int(box[0]),int(box[1])), (int(box[2]),int(box[3])), color, thickness)














class BoundingBox(object):
	pass

class ClassScores(object):
	pass

class ClassProbabilities(object):
	pass

class Detection(object):
	def __init__(self, box, objectness, scores, image_fn=(-1)):
		self.box = box
		self.objectness = objectness
		self.scores = scores
		self.object_key = -1
		self.image_fn = image_fn
	def has_object_key(self):
		return (self.object_key != -1)
	def get_object_key(self):
		return self.object_key
	def set_object_key(self, object_key):
		self.object_key = object_key
	def get_class(self):
		return np.argmax(self.scores)
	def get_box(self):
		return self.box
	def get_scores(self):
		return self.scores

class SampledDetection(object):
	pass

# class Detections(object):
# 	def __init__(self):
# 		pass
# 	def __iter__(self):
# 		return iter(self.detections)

class VideoDetections(object):
	def __init__(self):
		self.frames = {}
		self.classes = []

	@staticmethod
	def from_yolo(detections_path):
		video_detections = VideoDetections()
		data = json.load(open(detections_path))
		for image_detections in data['images']:
			video_detections.frames[str(image_detections['image_fn'])] = [Detection(det['box'], det['objectness'], det['scores'], str(image_detections['image_fn']))
																				for det in image_detections['detections']]
		video_detections.classes = [str(n) for n in data['classes']]
		return video_detections

	def per_image(self):
		image_fns = self.frames.keys()
		sort_nicely(image_fns)
		detections_per_image = [self.frames[k] for k in image_fns]
		return detections_per_image

	def at_image_fn(self, image_fn):
		if (self.frames.has_key(image_fn)):
			return self.frames[image_fn]
		return []

	def per_object_key(self):
		'''
			Returns a dictionary {instance_id: [detections]}
		'''
		per_object_key = {}
		for image_detections in self.frames.values():
			for detection in image_detections:
				if (not detection.has_object_key()):
					continue
				if (per_object_key.has_key(detection.get_object_key())):
					per_object_key[detection.get_object_key()].append(detection)
				else:
					per_object_key[detection.get_object_key()] = [detection]
		return per_object_key

	def n_detections(self):
		n_detections = 0
		for image_detections in self.frames.values():
			n_detections += len(image_detections)
		return n_detections

	def n_associated(self):
		n_associated = 0
		for object_detections in self.per_object_key().values():
			n_associated += len(object_detections)
		return n_associated


class System(object):
	def __init__(self, args):
		self.verbose = True
		if (self.verbose):
			print('[system] created new system object')
		self.protobuf_def_folder = args.protobuf_def.replace(args.protobuf_def.split('/')[-1], '')
		self.protobuf_def_file = args.protobuf_def.replace(self.protobuf_def_folder, '')
		self.dataset_folder = args.dataset_folder
		self.dataset_name = self.dataset_folder.split('/')[-1] if (not self.dataset_folder.endswith('/')) else self.dataset_folder.split('/')[-2]
		self.detections_folder = args.detections_folder

		# load protobuf definition
		sys.dont_write_bytecode = True
		sys.path.append(os.path.realpath(self.protobuf_def_folder))
		self.sn = importlib.import_module(self.protobuf_def_file.replace('.py',''))

		# parameters
		self.iou_threshold = 0.5

	def test_dataset(self):
		if (self.verbose):
			print('[system] testing dataset')

		# collect primary folders
		dataset_folder = self.dataset_folder
		primary_folders = [fn for fn in os.listdir(dataset_folder) if os.path.isdir(os.path.join(dataset_folder, fn))]
		sort_nicely(primary_folders)

		# get protobuf folder from dataset name
		root_path = os.path.realpath(os.path.join(dataset_folder, '../'))
		protobuf_folder = os.path.join(root_path, '{}_protobufs'.format(self.dataset_name))

		# loop over primary folders
		for primary_folder in primary_folders:

			# collect secondary folders
			secondary_folders = [fn for fn in os.listdir(os.path.join(dataset_folder, primary_folder))
										if os.path.isdir(os.path.join(dataset_folder, primary_folder, fn))]
			sort_nicely(secondary_folders)

			# collect protobuf from primary folder
			protobuf_name = 'scenenet_rgbd_{}_{}.pb'.format(self.dataset_name, primary_folder)
			protobuf_path = os.path.join(protobuf_folder, protobuf_name)

			# load protobuf
			if (self.verbose):
				print('[system] loading render protobuf ({})'.format(protobuf_name))
			trajectories = self.sn.Trajectories()
			with open(protobuf_path,'rb') as f:
				trajectories.ParseFromString(f.read())

			# loop over secondary folders
			for secondary_folder in secondary_folders:

				if (self.verbose):
					print('[system] coallating wnids from buff ({}/{})'.format(primary_folder, secondary_folder))

				# construct instance information
				instance_info = self.get_instance_info(trajectories, '{}/{}'.format(primary_folder, secondary_folder))

				# collect filenames
				images_folder = os.path.join(dataset_folder, primary_folder, secondary_folder, 'photo')
				instances_folder = os.path.join(dataset_folder, primary_folder, secondary_folder, 'instance')
				detections_name = '{}_{}_{}.json'.format(self.dataset_name, primary_folder, secondary_folder)
				detections_path = os.path.join(self.detections_folder, detections_name)

				# ensure detections exist
				if (not os.path.isfile(detections_path)):
					print('[warn] no detections @ ({})'.format(detections_path))
					continue

				# test video
				print('[system] testing video ({}/{}/{})'.format(self.dataset_name, primary_folder, secondary_folder))
				self.test_video(images_folder, instances_folder, detections_path, instance_info)

	def get_instance_info(self, trajectories, render_name):
		instance_info = {}
		trajectory = [t for t in trajectories.trajectories if (str(t.render_path)==render_name)]
		assert (len(trajectory) == 1)
		trajectory = trajectory[0]

		rejected_names = ['ceiling', 'floor', 'wall', 'light']

		for instance in trajectory.instances:
			if (str(instance.semantic_wordnet_id) == ''):
				continue
			if (str(instance.semantic_english) in rejected_names):
				continue
			else:
				instance_info[int(instance.instance_id)] = {'wnid': str(instance.semantic_wordnet_id),
															'name': str(instance.semantic_english)}

		self.accepted_instances = instance_info.keys()
		return instance_info

	def test_video(self, images_folder, instances_folder, detections_path, instance_info):
		# load detections
		video_detections = VideoDetections.from_yolo(detections_path)
		self.classes = video_detections.classes # TODO: not this

		# get image names from video
		image_fns = [fn for fn in os.listdir(images_folder)
			if os.path.isfile(os.path.join(images_folder, fn))] #.replace(fn.split('.')[-1],'')
		sort_nicely(image_fns)

		# loop over each image_data
		for image_n, image_fn in enumerate(image_fns):

			# get image detections
			image_detections = video_detections.at_image_fn(image_fn)

			# load rgb image
			rgb_path = os.path.join(images_folder, image_fn)
			rgb_image = cv2.imread(rgb_path)

			# load instance image
			instance_path = os.path.join(instances_folder, image_fn.replace('.jpg','.png'))
			instance_image = misc.imread(instance_path)

			# associate detections to instance_ids
			if (self.verbose):
				sys.stdout.write('[system] testing image {} ({}/{})\r'.format(image_fn, image_n, len(image_fns)))
				sys.stdout.flush()
			self.associate_detections(rgb_image, instance_image, image_detections)

		# print associations
		print('[video] finished with ({}) / ({}) detections associated'.format(video_detections.n_associated(), video_detections.n_detections()))

		# merge detections
		instance_scores = self.merge_detections(video_detections, instance_info, images_folder)

		# evaluate detections
		self.evaluate_detections(instance_scores, instance_info)
		print('\n')


	def merge_detections(self, associated_detections, instance_info, images_folder):
		instance_scores = {}

		# loop over objects
		for instance_id, object_detections in associated_detections.per_object_key().items():
			# sys.stdout.write('[dets] object {} with {} detections\r'.format(instance_id, len(object_detections)))
			# sys.stdout.flush()

			# average scores
			mean_scores = np.mean([detection.get_scores() for detection in object_detections], axis=0)
			instance_scores[instance_id] = mean_scores

			# print('[dets] object {}:{} with {} detections as {}'.format(instance_id, instance_info[instance_id]['name'], len(object_detections), self.classes[np.argmax(mean_scores)]))
			# for detection in object_detections:
			# 	print('    [dets] detected as {}'.format(self.classes[np.argmax(detection.get_scores())]))
			# print('')
			#
			# for detection in object_detections:
			# 	image_path = os.path.join(images_folder, detection.image_fn)
			# 	image = cv2.imread(image_path)
			# 	cv2_draw_box(image, detection.get_box(), (0,0,255), 2)
			# 	cv2.imshow('test', image)
			# 	cv2.waitKey(0)
		return instance_scores

	def evaluate_detections(self, instance_scores, instance_info):
		for instance_id, scores in instance_scores.items():
			instance_class = np.argmax(scores)
			instance_name = self.classes[instance_class]

			print('[eval] instance {}:{} detected as {}'.format(instance_id, instance_info[instance_id]['name'], instance_name))
			# instance_wnid = instance_info[instance_id].lstrip('0')


	def instance_box(self, instance_image, instance_id):
		# ensure instance exists
		if (instance_id not in np.unique(instance_image)):
			return None

		# get masks
		instance_mask = (instance_image == instance_id)
		horizontal_mask = np.any(instance_mask, axis=0)
		vertical_mask = np.any(instance_mask, axis=1)

		# calculate bounds
		xmin, xmax = np.where(horizontal_mask)[0][[0,-1]]
		ymin, ymax = np.where(vertical_mask)[0][[0,-1]]
		box = [xmin, ymin, xmax, ymax]

		# ensure box has width
		if (xmin == xmax or ymin == ymax):
			pass
			# print('this wasnt supposed to happen')
		return box

	def associate(self, detection, instance_boxes):
		instance_ious = np.array([iou(detection.get_box(), instance_box) for instance_box in instance_boxes])
		instance_ious[instance_ious < self.iou_threshold] = 0
		if (not np.any(instance_ious > 0)):
			return None
		return instance_ious.argmax()

	def associate_detections(self, rgb_image, instance_image, image_detections):
		# get instances in image
		instance_ids = np.unique(instance_image)
		instance_ids = [id for id in instance_ids if (id in self.accepted_instances)]
		instance_boxes = [self.instance_box(instance_image, instance_id) for instance_id in instance_ids]

		# loop over each detection in the image
		for detection in image_detections:

			# calculate matching instance
			match_index = self.associate(detection, instance_boxes)

			# check if successful
			if (match_index is None):
				continue

			# save association
			match_id = instance_ids[match_index]
			detection.set_object_key(match_id)

			# draw association




if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('--protobuf_def', help='Path to protobuf definition .py file',
			default='/media/feyre/DATA1/Datasets/SceneNetRGBD/pySceneNetRGBD/scenenet_pb2.py')

	parser.add_argument('--dataset_folder', help='Relative path to dataset',
			default='/media/feyre/DATA1/Datasets/SceneNetRGBD/pySceneNetRGBD/data/train/')
	parser.add_argument('--detections_folder', help='Relative path to folder of detections with naming (dataset_primary_secondary.json)',
			default='/home/feyre/git_ws/object_detectors/pytorch-yolo-v3-ayooshka-modified/detections/test_1')
	args = parser.parse_args()

	# my_system = System(args)
	# my_system.test_dataset()
	print('done !')

	dataset = SceneNetImages(args.dataset_folder, verbose=True)

	import time as timelib
	time_start = timelib.time()
	for image_i, (_, img, target) in enumerate(dataset):
		time_now = timelib.time()
		# print('took {:.5f}s'.format(time_now-time_start))
		print('[img {}]: targets: {}\n'.format(image_i, target))
		time_start = timelib.time()
		if (image_i > 10):
			exit()
	exit()

	from torch.utils.data import DataLoader
	torch.manual_seed(5)
	dataloader = torch.utils.data.DataLoader(
		dataset,
		batch_size=8,
		shuffle=True,
		num_workers=1,
		pin_memory=True,
		collate_fn=dataset.collate_fn,
	)
	import time as timelib
	time_start = timelib.time()
	for batch_i, (_, imgs, targets) in enumerate(dataloader):
		time_now = timelib.time()
		print('took {:.5f}s'.format(time_now-time_start))
		time_start = timelib.time()

	print('done x2')
